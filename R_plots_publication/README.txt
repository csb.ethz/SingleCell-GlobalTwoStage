This contains all the things related to the figures used in the manuscript

a) scripts: R scripts/ .ai files used are found in the respective folder
b) data: Data used in the figures is stored as .RData files in the data subfolder for all except the predictions of GTS in Llamosi's model which are .mat files. 
c) The supplementary figures can be generated using the accompanying .Rmd files in R and the figures are stored in the Sypplementary_figs folder.
d) The generated figures are either stored as pdfs/ or .Tex files. The .Tex files are compiled to pdfs using the TeX2pdfs scripts. The results are stored in the pdfs folder within each figure folder. 
e) The final main figures used are saved in Main_figs folder. 
