% Simulate a two-state model of gene expression
% Taken from https://github.com/nvictus/Gillespie
% Latest commit d772e07  on 29 Jun 2013
% http://ch.mathworks.com/matlabcentral/fileexchange/34707-gillespie-stochastic-simulation-algorithm
% All licesnses as in license.txt in the Gillespie

% Used by Lekshmi D

close all
clear all


%% Get the eterminsitic model
addpath(genpath('~/TOOLBOXES_MATLAB/odeSD'))
addpath(genpath('../Gillespie/'))
%Model=generateSymbolicRepresentation('/Model_twostate.txt');
%createOdeSdCRhs(Model, '', '');
%odeSD_compile_hybrid('Model.c', 'Model', './');

rate_protein= [1.5,2,1.75];
rate_mRNA=[0.3,0.05,0.2];
t=0:10000;

% model name and jacobian
jacfun='@jac_Model_twostate';
rhsfun='@Model_twostate';
options=odeset('Jacobian',jacfun);

% simulate cell A
[t_a,cell_a_y]=odeSD_wrapper('Model_twostate',t,[0,0]',options,[rate_mRNA(1),rate_protein(1),0.03,0.002]');

% simulate cell B
[t_b,cell_b_y]=odeSD_wrapper('Model_twostate',t,[0,0]',options,[rate_mRNA(2),rate_protein(2),0.03,0.002]');

% Simulate cell C
[t_c,cell_c_y]=odeSD_wrapper('Model_twostate',t,[0,0]',options,[rate_mRNA(3),rate_protein(3),0.03,0.002]');

%% Simulate Gillespie for three cells
seeds={[3],[50],[100]}; %
cols={'k','b','g'};
sh={'+','*','o'};
file_to_save='../box1_fig';
noise_param.theta0=10;
noise_param.theta1=0.1;
ssa_example(seeds,rate_protein, rate_mRNA,noise_param,cols,sh,file_to_save)


% plots
% determinsitic protein concentrations
subplot(1,3,2)
plot(t_a/3600,cell_a_y(:,2),'k')
hold on
plot(t_b/3600,cell_b_y(:,2),'b')
plot(t_c/3600,cell_c_y(:,2),'g')
ylim([0,9000])
xlim([0 1])
set (gca,'color','none')
% determinsitic mRNA concentrations
subplot(1,3,1)
plot(t_a/3600,cell_a_y(:,1),'k')
hold on
plot(t_b/3600,cell_b_y(:,1),'b')
plot(t_c/3600,cell_c_y(:,1),'g')
xlim([0 1])
set (gca,'color','none')
% uncomment to save, after modifying
% print -painters  -dpdf -bestfit fig1_cut.pdf

subplot(1,3,3)
set (gca,'color','none')

%% Generate  the fucturations

figure()
% Measurement noise
subplot(1,3,1)
plot([1:10000]./3600,normrnd(0,10,10000,1))
xlim([0,1])
xlabel('Time (h)')
ylabel('Random fluctuations')
set (gca,'color','none')
% HYPOTHETICAL FLUCTUATIONS RNAPOL
subplot(1,3,2)
rng(10)
y1=plot(1:300,poissrnd(50,300,1).*[300:-0.5:150.5]');
hold on
rng(1000)
y2=plot(1:300,poissrnd(50,300,1).*[300]');
xlim([200,300])
set (gca,'color','none')

% HYPOTHETICAL FLUCTUATIONS RIBOSOMES
subplot(1,3,3)
rng(1)
y1=plot(poissrnd(50,300,1));
hold on
rng(100)
y2=plot(poissrnd(100,300,1));
xlim([200,300])
set (gca,'color','none')

print -painters  -dpdf -bestfit fig2_cut.pdf