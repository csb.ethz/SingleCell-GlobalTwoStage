function res=OLS(t,kinetic_params,d,int_opts,model_fun)
%%%------------------------------------------------------------------------
%
%   function OLS.m
%
%   Standard OLS (Ordinary Least Squares) method as in the Davidan book:
%   Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%   1996 Reprint edition 
%
%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%		  	kinetic_params: log kinetic parameters
%			d: data
%		int_opts: Options passed to integrator, structure.
%       model_fun: function handler to the ODE model

%  OUTPUT==> res: The objective function ordinary least squares. 

%   Lekshmi Dharmarajan, July 2017
%   Checked January 2018

%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

nonans=~isnan(d); % Are there NaN values in data?

% If any paramater is infinity, then return the objective value to be large. 
if any(exp(kinetic_params)==Inf)
res=Inf;
else
[f_t]=model_fun_user(t,exp(kinetic_params),int_opts);

% OLS estimate, only use res if there are no Infinite values in the ODE solution. 
if (any(f_t==Inf))	% if (any(f_t==Inf))
res=Inf;
else
res=(d(nonans)-f_t(nonans))'*(d(nonans)-f_t(nonans));
end
end

