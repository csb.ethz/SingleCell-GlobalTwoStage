function FirstStage_TEMPLATE()
%%%------------------------------------------------------------------------
%
%   function FirstStage_TEMPLATE.m
%   Implements the First stage for the analysis of the data from Llamosi et al, 2016. Reads in 
%   the data used in Data/ and saves the results in output folder.
%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%       qsub submit_FirstStage.sh
%   The resulting mat files store a lot of information, the estimates, function values, return code,
%   data and model simulation for each sell in a structure 'GLS_params'. Followed by information on the convergence of parameters, CPU time 'CPU_end' and 
%   noise parameters ('theta_hat', 'sigma2') and the individual FIMs 'C_inv'. 
%   Tools needed: 
%         a) ODE SOLVER: #USER# must provide to inetgrate Model_user.m
%         b) Optimizer:  #USER# must provide (ALGORITHM_user)
%   Funtions, objective functions used: 
%         a) OLS estimates
%         b) WLS
%         c) POOLED GLS 
%         d) Model integration (@model_fn_user, and evaluating the error model (noise_model_user.m)  #USER# must provide
%         e) Auxilliary functions to calculate the fisher information matrix (getFIM_user). #USER# must provide
%   
%   Lines that have to modified:
%         a) the paths to ODE integrator, and Optimizer. 
%         b) Output directory
%         c) parallellization options

%   Lekshmi Dharmarajan, July 2017
%   Checked, January 2018
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

    addpath(genpath('#USER#'))                      % path to ODESD, ODE SOLVER (USER MUST MODIFY) 
    addpath(genpath('Model/'))                      % path to ODE model, noise model
    addpath(genpath('#USER#'))                      % path to Optimizer(USER MODIFIABLE)
    addpath(genpath('Auxilliary'))                  % path to helper functions
    addpath(genpath('FirstStage_objs'))             % path to objective functions used
    addpath(genpath('Data'))                        % path to objective functions used

   
    % Output directory
    out_dir='Pubs_Results';                         % path to store the results (USER MUST MODIFY)

    % Parallel ability: To enable parralelisation, start parpool
    c = parcluster('local'); % Change 'local' to any other cluster profile (USER MUST MODIFY)
    c.NumWorkers = #USER#;
    parpool(c, c.NumWorkers);
    

    % Load the data: data should contain columns Y, ID and TIME at the most. 
    % and can be modified.
    data = readtable('#USER#','Delimiter',',', 'TreatAsEmpty','NA');
    ids = unique(data.ID);

    %%%%%%%%%% Tweaking parameters: %%%%%%%%%%
    % Initial parameters settings:
    p0=#USER#
    noise_params0=#USER#
    % Initialise
    beta_init = p0;
    theta_init = noise_params0;
    n_cells=length(ids);
    n_kinetic_params=length(beta_init);

    % Parameters that can be tuned:
    % tolerances for optimizer convergence
    x_tol=1e-5;
    f_tol=1e-5;
    x_tol_gls=1e-5;
    f_tol_gls=1e-5;

    % tolerances to the ODE SOLVER #USER#
    int_opts=struct();
    int_opts.abstol=1e-8; 
    int_opts.reltol=1e-4;

    % Convergences
    max_iter_GLS=10;
    min_iter_GLS=2;
    max_score=0.01;

    % Objective function used to estimate the noise parameters
    residual_type='AR'; % or choose 'PL'

    % Pre initialze the array to store the results of the individual fits. 
    x_ols = zeros(length(ids), length(beta_init));  % Store individual parameters
    fval_ols = zeros(1, length(ids));               % Store the function values
    conv = [];                                      % Convergence 

    % model funtion handle that should be passed into the functions 
    % #USER# Must modify
    model_fn_user=@(time, params,options) Model_user(time, params, options);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % OLS estimate:
    t1 = tic
    spmd, cpu0 = cputime(); end
   parfor i = 1: n_cells
        t = data.TIME(data.ID == ids(i));
        % NOTE: MAY NEED MODIFICATION. 'para' is in log space, and the input to the model is not in log space
        % a transformation is done in the objective function 

        fun = @(para) OLS(t, [para], data.Y(data.ID == ids(i)),int_opts,model_fn_user); 

        % Optimization settings
        opt = struct();
        opt.algorithm = ALGORITHM_user;
        opt.min_objective = fun;
        opt.ftol_rel = f_tol;
        opt.xtol_rel = x_tol;
        [x_ols(i, :), fval_ols(i), exflg(i)] = nlopt_optimize(opt, beta_init);
    end
    t2 = toc(t1);

    %% Initialise with OLS estimate: beta_init. 
    beta_init = x_ols;

    % iteration counters
    itern = 0; 
    iternact=0; 


    % estimates in the first iteration
    betai_gls = beta_init;
    theta_gls = [theta_init];

    % Convergence score.
    score = 10^5; % Initialise the score to a large number      
    diffs = []; % to store error

    % To store parameters
    diff_noise = [];   
    diff_p1 = [];
    diff_p2 = [];
    diff_p3 = [];
    diff_fval = [];

    % Start GLS-POOL estimation: 
    % #USER# can set the convergence criteria differently
    t3 = tic;
    while (itern == 0 || ((itern <= max_iter_GLS-min_iter_GLS) && (score >= max_score)))  % Convergence criteria
	
	for_pl=struct();
	% Evaluate all the model simulations at the optimum
	parfor i=1:n_cells
           t = data.TIME(data.ID == ids(i), : );
      	   f_t=model_fn_user(t,exp(betai_gls(i,:)),int_opts);#USER#
	       for_pl(i).f_t=f_t;
	end

    % Maximize noise parameters with
        if residual_type=='AR'
        fun = @(para) get_AR_gm(for_pl,  para, data, ids);
        else
        fun = @(para) get_PL_gm(for_pl,  para, data, ids);
        end

        opt_noise = struct();
        opt_noise.algorithm = ALGORITHM_user;
        opt_noise.min_objective = fun;
        opt_noise.ftol_rel = f_tol;
        opt_noise.xtol_rel = x_tol;
        [theta_hat, ~,~] = nlopt_optimize(opt_noise, theta_gls); 
        
        % initalizeing the array to store result
        betai_hat = zeros(length(ids), size(betai_gls, 2));

        % Construct weights with theta_hat and maximize parameters
        parfor i = 1: n_cells
            t = data.TIME(data.ID == ids(i), : );	
	
		% get weights
	   h=noise_model_user(theta_hat,for_pl(i).f_t); 	
	
		% Re-estimate individual parameters
	    fun = @(para) WOLS(t, [para],  h, data.Y(data.ID == ids(i), : ),int_opts,model_fn_user); 
            opt = struct();
            opt.algorithm = ALGORITHM_user;
            opt.min_objective = fun;
            
            % Using low tolerances here can lead to bad estimates.
            opt.ftol_rel = f_tol_gls; 
            opt.xtol_rel = x_tol_gls;
            [betai_hat(i, :), fval(i), exflg(i)] = nlopt_optimize(opt, betai_gls(i, : ));
        end

        % Get the convergence measurement and store, based on relative convergence
        score1 = max(max(abs((betai_hat - betai_gls)./ betai_gls))); 
        score2 = max(abs(theta_gls - theta_hat)./theta_gls); 
        score = min(score1, score2);
        diffs = cat(1, diffs, cat(2, score2, score1));

        % Save the values of the parameters in different steps #USER#
        diff_noise = cat(1, diff_noise, theta_hat);
        diff_fval = cat(2, diff_fval, fval);
        % disp(diffs)

        % Set for iterator
        iternact = iternact + 1;
        itern = max(0, iternact - 2);
        betai_gls = betai_hat;
        theta_gls = theta_hat;

    end


    %%
    % Calculate the sigma from the residuals
    for_pl = struct();
    for i = 1: n_cells
        p =  exp(betai_hat(i,:));
        t = data.TIME(data.ID == ids(i), : );
        for_pl(i).f_t=model_fn_user(t,p,int_opts);
        [for_pl(i).res, for_pl(i).h, for_pl(i).gm] = WLS_gm(for_pl(i).f_t,theta_hat, data.Y(data.ID == ids(i), : ));
    end

    % Calculate the final estimate of sigma2 
    sigma = (cell2mat({for_pl.res}')./cell2mat({for_pl.h}')).^2;
    sigma2 = sum(sigma(: )) / (length(sigma(: )) - size(betai_gls, 2) * size(betai_gls, 1));

    % Make directory to save results, and save all results in a structure and some variables in the MAT file. 
    mkdir(sprintf('%s',out_dir))
    GLS_params=struct();

    % 
    % Saving model simulations ... and settings used
    % Get the FIM/ Hessian of the estimate!
    C_inv=zeros(n_kinetic_params,n_kinetic_params,n_cells);     % Storing FIM.
    C_inv_hessian=C_inv;                                        % Storing Hessian
    
    for i=1:n_cells
    GLS_params(i).t=data.TIME(data.ID==ids(i),:);   % Time
    GLS_params(i).dat=data.Y(data.ID==ids(i),:);    % data
    GLS_params(i).params=betai_hat(i,:);            % estimates of parameters
    noise_params=sqrt(sigma2).*[theta_hat(1) exp(theta_hat(2))]; %#USER# modify based on noise model
    % Can be avoided if the optimzer returned the FIM/Hessian
    [C_inv] = getFIM_user(t,exp(betai_hat(i,:)),GLS_params(i).dat,noise_params);
    GLS_params(i).C_inv=C_inv(:,:,i);               % FIM/ Hessian             
    GLS_params(i).theta_hat=theta_hat;              % Noise estimates: Pooled 
    GLS_params(i).sigma2=sigma2;                    % Noise estimate:  Pooled, can be amended if the pooled assumption is not true 
    GLS_params(i).int_opts=int_opts;                % Integration options
    GLS_params(i).exflg=exflg(i);                   % Exit flags of the individual optimization of cells    
    GLS_params(i).fval=fval(i); 
    end

    t4 = toc(t3);
    spmd, cpu_end = cputime() - cpu0, end
    spmd, cptime = gplus(cpu_end), end
    cpu_end = cell2mat(cpu_end(: ));
    cptime = cell2mat(cptime(: ));

    mkdir(out_dir);
    % Save results                                  % (USER MUST MODIFY)
save(sprintf('%s/%s_Result.mat', out_dir,residual_type), 'GLS_params','cpu_end', 'theta_hat','cptime','betai_hat','C_inv','sigma2','int_opts'
    % end parallelization. 
    delete(gcp('nocreate'))
    diary off
end
