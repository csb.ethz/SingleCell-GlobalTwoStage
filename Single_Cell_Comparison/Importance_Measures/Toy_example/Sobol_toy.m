% Calculating Sobol indices for a toy example. 
% Toy example: 
%       y(x1,x2)=x1^2
% Domain: multivariate standard normal with NO correlations
% Lekshmi D
% Checked in March 2018

% Model
fun_y=@(x1,x2) x1.*2;

mu=[0,0];
sigma=[1 0;0 1];
n_samples=1000;

rng('shuffle')
% 
x_samples=mvnrnd(mu,sigma,n_samples);

%% Get the unconditional variance of Y 
y_samples=fun_y(x_samples(:,1),x_samples(:,2));
V_y=var(y_samples);

%% Calculating S_p for the xi' th parameter
% calculate V_xi'E_xi(Y|X_xi') % Variance over xi'
% calculte E_xi(Y|X_xi')       % Expectation of xi


% Variance over x1
all_exps=[];
V_T=zeros(1,2);
for i=1:n_samples
x_nsample=x_samples(i,1); % treattin the first sample fixed. 
mu_samples=mu;
sigma_samples=sigma;
sigma_samples(1)=0;
mu_samples(1)=x_nsample;
x_samples1=mvnrnd(mu_samples,sigma_samples,n_samples);
y_samples1=fun_y(x_samples1(:,1),x_samples1(:,2));
all_exps=cat(1,all_exps,mean(y_samples1));

end
V_T(1)=var(all_exps);

% now for second x_2
all_exps=[];

for i=1:n_samples
x_nsample=x_samples(i,2); % treattin the second sample fixed. 
mu_samples=mu;
sigma_samples=sigma;
sigma_samples(2,2)=0;
mu_samples(2)=x_nsample;
x_samples1=mvnrnd(mu_samples,sigma_samples,n_samples);
y_samples1=fun_y(x_samples1(:,1),x_samples1(:,2));
all_exps=cat(1,all_exps,mean(y_samples1));

end
V_T(2)=var(all_exps);
S_p=V_T./V_y;

% FOr this boring example, only x1 should matter and that is what we see. 
S_p


