function outputs = get_outputs(params,time,int_tol,observation_id)
outputs=zeros(size(params,1),length(time));
for i=1:size(params,1) % parallelize this loop... The method depends on how well we accurately had
                       % sampled. 
[obs_y]=Run_Llamosi2016([0,time],exp(params(i,:)),int_tol);
for j=observation_id
    if any(obs_y<0)
        disp('negative states')
    end
    if isempty(obs_y) % integration had failed.
        c1=NaN;
    else
c1=obs_y(2:end);
    end
end
outputs(i,:)=c1';

end