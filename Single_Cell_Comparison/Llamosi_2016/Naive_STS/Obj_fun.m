function res=Obj_fun(t,para,d,int_opts)
%%%------------------------------------------------------------------------
%   function Obj_fun.m
%   Generates the predictions for the results from GTS. And saves it as an array, 
%   where each coloumn is the time and the rows are independent simulated trajectories. 
% 
%   Tools needed: 
%         a) ODE SOLVER
% 
%   Funtions, objective functions used: 
%         c) Model integration
%   
%   INPUT==> t: experimental time of a cell
%            para: all parameters
%            d: data for a cell
%           int_opts: integration options
%
%   OUTPUT==> res: Normal likelihood objective function

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%

e_a=para(4);        %error term- additive
e_b=para(5);   %error term- multiplicative

nonans=~isnan(d);
% Integrate ODE model and get noise model
if any(exp(para([1,2,3,5]))==Inf)
    res=Inf;
else
[f_t]=Run_Llamosi2016(t,exp(para(1:3)),int_opts);
h=var_fun([e_a,e_b],f_t);

% Normal likelihood
if (any(h==Inf)||sum(log(h)==-Inf)||~any(isreal(log(h))))
    res=Inf;
    else
    res=-(-0.5*nansum(((d(nonans)-f_t(nonans))./h(nonans)).^2)-sum(log(h(nonans))));
end

end
%%%------------------------------------------------------------------------
