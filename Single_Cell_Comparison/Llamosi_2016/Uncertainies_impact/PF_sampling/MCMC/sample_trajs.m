function sample_trajs(input_id)
%%%------------------------------------------------------------------------
%   function sample_trajs.m
%
%   Sampling using DRAM
%   The goal is to verify the linearisation approximation to calculate the
%   uncertainities. Uses DRAM software.
% Cite: 
% 
%    REQUIRES THE DRAM software
%    
%   Input: 
         % cell id. 
% 
%    Saves: 
%       1e4 samples for the cell, under 'temp',
%       the value of the objective function used for each sample uder 'obj_val'
%       time elapsed under 'time_elapsed'

%   Lines to be modified by user:
        % input file, and where to save
        
%   Lekshmi Dharmarajan, January 2018
%   CSB, ETH Zurich. 
%
%   To submit the job in an SGE cluster use the submit script: scubmit_sc: 
%    qsub -t 1-325 submit_sc.sh
%
%%%-----------------------------------------------------------------------
    addpath(genpath('/usr/local/stelling/grid/matlab'))
    addpath('./mcmcstat/')
    addpath(genpath('~/Repos/odeSD/'))                    % path to ODESD, ODE SOLVER (USER MUST MODIFY)
    addpath(genpath('../../../Model/'))                % path to ODE model, variance model
    addpath(genpath('~/TOOLBOXES/nlopt-2.4.2/'))          % path to NLOPT (USER MODIFIABLE)
    addpath(genpath('../../../Auxilliary'))                  % path to helper functions
    addpath(genpath('../../../Pub_Results'))                 % path to objective functions used
    addpath(genpath('../../../Data')) 
    

%% Get the data
data = readtable('../../../Data/Di_monolix_full.csv','Delimiter',',', 'TreatAsEmpty','NA');
ids = unique(data.ID);

% Result file
load('../../../Pubs_Results/AR_Result.mat')    % modify this
outdir='./Samples_Results';                 % folder to store result, modify this

theta_hat=theta_hat;
betai_hat=betai_hat;
C_inv=C_inv;
sigma2=sigma2;

% time taken
t1=tic();

% To avoid any pathological space
domains_max=max(betai_hat)+log(5);
domains_min=min(betai_hat)-log(5);
%domains_min=log([ 1e-6 1e-6 1e-6]);
%domains_max=log([1e6 1e6 1e6]);

% Initialising samples
nsamp=1e4;
psampMH=zeros(nsamp,3,length(ids));
obj_val=zeros(nsamp,1);

for cell_num=input_id
    int_opts=[];
    model=[];
    options=[];
    params=[];
    
id_cell=ids(cell_num);
data_cell=data.Y(data.ID==id_cell);
data_time=data.TIME(data.ID==id_cell);
ind_est=betai_hat(id_cell,:);
C_est=C_inv(:,:,id_cell);
int_opts.abstol=1e-4;
int_opts.reltol=1e-4;
noise_params=[theta_hat(1)*sqrt(sigma2) exp(theta_hat(2))*sqrt(sigma2)];

% noise model used
[yy]=Run_Llamosi2016wosens(data_time,exp(ind_est),int_opts);  % no sensitivities integrated
noise_est=noise_params(1)+yy.*noise_params(2);

%%
% Target distribution.
fun=@(para) -2*(WOLS(data_time,para,noise_est,data_cell,int_opts));

model.ssfun     = fun;
model.N         = 1;
options.method  = 'dram';
options.burnintime=100; 
Covs=get_stableinverse(C_est);
options.nsimu   = nsamp;
%qcov=corr2cov([1 1 1],cov2cor(Covs));
%qcov=eye(3).*0.5;
qcov=Covs;
options.qcov    =qcov;


params{1}={'km',ind_est(1),domains_min(1),domains_max(1)};
params{2}={'gm',ind_est(2),domains_min(2),domains_max(2)};
params{3}={'gp',ind_est(3),domains_min(3),domains_max(3)};

[~,psampMH(:,:,cell_num)] = mcmcrun(model,[],params,options);

temp=psampMH(:,:,cell_num);

for i=1:nsamp
obj_val(i,1)=fun(temp(i,:));
end

end

time_elapsed=toc(t1);
mkdir(outdir) %modify this
asymcov_lin=Covs;
asymcov_samp=cov(temp);

save(sprintf('./Samples_Results/psampMH%d.mat',cell_num),'params','qcov','temp','obj_val','time_elapsed','asymcov_lin','asymcov_samp');




