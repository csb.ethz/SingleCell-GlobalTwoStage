Contains sampling using DRAM in PF_sampling/MCMC 
		The implementation of DRAM is taken from http://helios.fmi.fi/~lainema/dram/ 
		H. Haario, M. Laine, A. Mira and E. Saksman, 2006. DRAM: Efficient adaptive MCMC, Statistics and Computing 16, pp. 339-354. (doi:10.1007/s11222-006-9438-0) 

---------------------
Tools needed: 
---------------------
	1. DRAM (mcmcstat folder) (H. Haario, M. Laine, A. Mira and E. Saksman, 2006. DRAM: Efficient adaptive MCMC, Statistics and Computing 16, pp. 339-354. (doi:10.1007/s11222-006-9438-0) )
	2. odeSD for integration
	3. Model files. 

---------------------	
Main files: 
---------------------
	Relevant matlab file sample_trajs.m 
	uses WOLS.m as the likelihood function. 
	
	Note that the noise model is fixed to that estimated form GTS. 
	
	Results are stored in Samples_Results. 

---------------------
BASH scripts: 
---------------------
submit_sc.sh submits the sampling job for each cell in the SGE cluster. 
embarassingly parallel. 



