/******************************
* This file is part of odeSD. *
******************************/ 

/* Standard header files. */ 
#include <stdlib.h> 
#include <string.h> 
#include <math.h> 

/* Library header files. */ 
#include <clapack.h> 
#include <cblas.h> 

/* Initial conditions. */ 
const int Llamosi2016_Nstates = 3; 
const int Llamosi2016_Nparams = 4; 
const double Llamosi2016_x0[3] = {0., 0., 0.};
const double Llamosi2016_p[4] = {4., 0.008, 0.008, 3.e+01};
const char *Llamosi2016_name = "Llamosi2016";

/*****************************************
* Jacobian Right-hand side for ODE model *
*****************************************/

int Llamosi2016_jac ( double time , const double *x , const double *f , const double *p , void *varargin , double *J , double *dJ , double *dfdp , double *d2fdtdp ) 
{ 
	/*******************************
	* STATE JACOBIAN EQUATIONS (J) *
	*******************************/ 
	
	if ( J != NULL || dJ != NULL || d2fdtdp != NULL ) 
	{ 
		memset( J , 0.0, sizeof(double) * Llamosi2016_Nstates * Llamosi2016_Nstates );
		
		J[0] = p[1]*-1.0;
		J[1] = 9.47E-1;
		J[4] = p[2]*-1.0;
		J[6] = p[0];
		J[8] = -9.225E-1;

	} 

	/*********************************************
	* JACOBIAN OF 2nd DERIVATIVE EQUATIONS (Jgx) *
	*********************************************/

	if ( dJ != NULL )
    { 
		memset( dJ , 0.0, sizeof(double) * Llamosi2016_Nstates * Llamosi2016_Nstates ); 
        


		cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , Llamosi2016_Nstates , Llamosi2016_Nstates , 
		Llamosi2016_Nstates , 1.0, J , 
		Llamosi2016_Nstates , J , Llamosi2016_Nstates , 
		1.0 , dJ , Llamosi2016_Nstates );  
	 } 


    /************************************** 
    * PARAMETER JACOBIAN EQUATIONS (dfdp) * 
    **************************************/

    if ( dfdp != NULL || d2fdtdp != NULL )
    { 
        memset( dfdp , 0.0, sizeof(double) * Llamosi2016_Nstates * Llamosi2016_Nparams ); 
		dfdp[0] = x[2];
		dfdp[3] = x[0]*-1.0;
		dfdp[7] = x[1]*-1.0;

    } 

    /****************************************************
    * 2nd derivative PARAMETER JACOBIAN EQUATIONS (Jgp) *
    ****************************************************/ 

    if ( d2fdtdp != NULL )
    { 
        memset( d2fdtdp , 0.0, sizeof(double) * Llamosi2016_Nstates * Llamosi2016_Nparams ); 

		d2fdtdp[0] += f[2];
		d2fdtdp[3] += f[0]*-1.0;
		d2fdtdp[7] += f[1]*-1.0;


        cblas_dgemm( CblasColMajor , CblasNoTrans , CblasNoTrans , Llamosi2016_Nstates , Llamosi2016_Nparams , 
        Llamosi2016_Nstates , 1.0, J , 
        Llamosi2016_Nstates , dfdp , Llamosi2016_Nstates , 
        1.0 , d2fdtdp , Llamosi2016_Nstates );  
    } 


 /* All is well that ends well... */ 
 return 0; 

} 


/********************************
* Right-hand side for ODE model *
********************************/

int Llamosi2016_f ( double time , const double *x , const double *p , void *varargin , double *f , double *dfdt , double *J , double *dJ , double *dfdp , double *d2fdtdp )
{ 
	int res = 0;

	/*******************************
	* 1st DERIVATIVE EQUATIONS (f) *
	*******************************/

		f[0] = p[1]*x[0]*-1.0+p[0]*x[2];
		f[1] = x[0]*9.47E-1-p[2]*x[1]*1.0;
		f[2] = x[2]*-9.225E-1+((tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1+5.0E-1)*(p[3]-time*1.0+2.0)+(tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1-5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-(tanh(p[3]*1.0E2-time*1.0E2+1.4E3)*5.0E-1+5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-5.0E-1)*(p[3]*2.5E-1-time*2.5E-1+3.5)*1.0+5.0E-1))*(tanh(p[3]*1.0E2-time*1.0E2+2.0E2)*5.0E-1-5.0E-1)*3.968E-1;


	/***************************************
	* 2nd DERIVATIVE EQUATIONS (Jfx*f = g) *  
	****************************************/

	if ( dfdt != NULL ) 
	{ 
		dfdt[0] = f[0]*p[1]*-1.0+f[2]*p[0];
		dfdt[1] = f[0]*9.47E-1-f[1]*p[2]*1.0;
		dfdt[2] = f[2]*-9.225E-1;

	} 

	/* Jacobians and stuff. */ 
	if ( ( res = Llamosi2016_jac(time , x , f , p , varargin , J , dJ , dfdp , d2fdtdp ) ) < 0 ) 
	return res; 

	/* All is well... */ 
	return 0; 
 } 

