% symbolic derivation of sensitivities of the output 
syms km gm gp tau  x(km,gm,gp)
y=x*exp(-gp*tau);
dydx=jacobian(y,[x]);
dydp=(jacobian(y,[km,gm,gp]));
d2ydp2=jacobian(jacobian(y,[km,gm,gp]),[km,gm,gp]);

