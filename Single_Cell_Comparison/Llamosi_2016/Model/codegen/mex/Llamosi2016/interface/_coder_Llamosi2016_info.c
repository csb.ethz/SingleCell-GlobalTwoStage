/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_info.c
 *
 * Code generation for function '_coder_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016.h"
#include "_coder_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(6.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[17] = {
    "789ced5c4d6ce3c615a68bad9b20489a064882a46ddabd042d1631e595d6916ed69f65adad3f4bb2bd5a240a450ea53187331487fab14f7b2cd0431304411204"
    "05f65a14e825018ade8af6d24b6f49905b2eb9f4dc4b8fe5e8c7966799a557946866290284f428bde1f71ee7e37b6f6648612d5f10eced057bffdf3b82b06e7f",
    "3e63ef3f12c6db8f27f29abdff7cf2393e7e43787e22ffc1de65822d30b4c63f624907c27453880eb184addaa90104135082fa4019fda242046a5007fb6446d8"
    "85b6a0efccfc742eb09fd8f77407c85ab5a70b66879e9f4640b3c2c89e4f840b7b6e38d823cdd8f3e244be9f7d47ec101d880a021aede850dc47924e68d330c5",
    "24424d192044c57c5717ab10b71168a6ed03cd34d10dc98494e0c9df61f37664734b2c1005a0e9217664439fc11771c177e312be1b8225e10ed38bbbe8ad7376"
    "ad8fae42af85c0f8bcbf73d1af72fa4cbe9fdf3fb65d838125b6e85b6d132a7420d2c15b2630887d8402b190aced2753cd0366664bb408412d3214818e44045b",
    "a22e59486a8900a93d2c3243269ee0ed5977c0b33683e7d9c9717bfbd3f69b5f253de87b3dffc2f47785c75f8f5784cbd783c95b30d78f664a719c360a6f03e9"
    "a8978a9a673be3f67e33d3de9a437bc2cce73cff0f5aff29bbe0799dc3c364d966a6b901ed9b968925b441650949e6066bf8c2cec7f173bacdb63bd57bdf054f",
    "93d36b7af40fdb6f8d0c126f4d2d126f8d4d9afa6afe7efae0cffff947787966ece16c1cec35a4ea5e712ba1f5c1ed612f9f0e27cfde73c1f35b0e0f93399e49"
    "86814eaba39eb9d3c3b20509cee33292e469bec0ceb3ed729e9f72e761b23a69add991b01dba47ed3c746947e7dad13dfacf9187df6ff1c8b31efa75e3db6878",
    "7919c3757c07c606112a55496b006a5a5cca64fce1e54317bc41e957cb8a8b5ef3cf557c7c7a78789df131e8f5ddb60b3ea7387622c9cd99f616c237373f014e"
    "1f2cc94f9c6d1ee3dfdf3f3b91be0e2defce8c5887a43415d1484dd3770a19a32ddf0908ef82d49f9cf03dc3e163b26e2ab00f15e0c7f84a9dd367f282f27662",
    "50716aca86e1e08f27886b7ffde377d71ad73e4dbdb698f3cfc3af41f4d03c2d16ebb2469249b35deda5e2ca5140eabeebec3f4e787ec2e161f2a48d85c42f37"
    "7fd438fdda62fd3175873ebd3e73f2e9c3f54a38f2c497b9ebc1e4f8a0a2d68f635b6feff6d14122138fef93ba2c04834f7ef71fb7baec571c1e26737519a4a9",
    "1e44561e177b3a30a1ec4b9e2873fab2473f39d665bc65dee709be78e1df5f8682774e712c83e0a614cfb47762ad442fae94b0be952aefae78e784e7d71c1e26"
    "3b8c8780a131ca4b2d6833c70fde299cbee2d14f8ebc7bc432aff14ef87051f9db75ebcfc3bbd6f05ef14095b4c34a61af78f70058d548b51452deb9f9ef250e",
    "0f9339ded94d5ed8b7ec7cf290d33ff4e81f47bed916310f79c8277f1f967cd2895f85ba15b9973f4a625c3f80655a29b669ad9b5d8d7f5cc59f3fe3f031d9ee"
    "a34ddd823aa0cd0e40063067ec5dd63cf8bb9c1e9317781f7ac4248f7cfbf2f3bf85976f8df69e9a846759506e67b2898a5eb91ba1b95c30e6db108797c9cbed",
    "473765a2eb043765b6128d4e71aeeaba555d1786ba2ea8f775373fbecae162b2e37c77b6edcb3cf77d4effbe473f7d6f5dc72cf2ceb717bf298537fe9572b974"
    "3756aa52b47974d4dd525a1456a04ff30141ed47db2eb89ee7703119523cbeff5b6c39b83feb491a9c7ec3a37fd8fd488543a018c4768f78c9a4e902000ffdf4",
    "a3308f9be4a2da69b5db86285d4b6b07dd54bba6d5b33eadeb0a2acfe699cf86548576ccf4e5798165cc475ee2d7d814afeb2543c32ba7f9b77e17648dbe4c13"
    "c7832821655add8cde6b08c1182ff1a5feb8e0d54d159ad452e1393eb7baed0d0e1f9359126ab7dd54898908319aa40f4c1591c1b824f4366ef2d0058fc6e969",
    "1efd35bbee7becbec798e6398f7cf0715878e814df487ad8ada88546b263928c99ddb78e0685864f79e4bf5cf07ec0e165b28ffdeae6e3fff064e3926bc2a3cf"
    "cfb140b2a03a1162050cf3d8ba527cac70ed5516e0572e3e5e5a77ba8a8f4fcecbf2ed5ae4ac7c8a8f87dd93e8e9dddd935ab5d5d80dc67c9ddffd67db05cf55",
    "ebba27e5a7dc91cc553df788bed7f32f4cff8756cf3d6df16e9e3ace6e5f9786def8781df5dff97388f617716c84d7fa2fd479271a6e3540e4a414a9c354a436"
    "38db9462c8aff982150fc73c84f829e021c42b1e0af3f3f04ebea0195a110caddd42b55dceed957311945a8dc378a9e7963dbeb9aadf82cfabebacdf823a1fee",
    "36aef91a878bc9dc38470b497463d806ba97f7165db5ae5bb49f9c9ffb66268923933cae07fbcb9bef87976f1d1dc5d5782ca569890a50e55a2f7a0c92d9a793"
    "6f57ed470b78dfc2886f102388c185bdcb5a7fe9cf7b16467e1a9be4e93d0bc21bff7d10def55ea99694a4a962acdc47f10ab6acf891d6edfab4fef287cab72b",
    "acb71cf1ad4741d3042afbee896f81788e67eca719933c3fc7136ade99c942ac9a54d325394be9897498ef2432ba4ff3750f5df0620e2f9397cbbb9b2dc0c645"
    "ac8e09688720e56a3cfc258793c94e3c9c69d6539ee9c6c31687a7b53cbf9d9bb480798474e49fe1cd370bca61a2548cf7723b7795629794caf5cc5934e4f9a6",
    "9b1f57eb9d57eb9de7e55b90d73b0735dffc05878bc91cdfa639d9f990ca32e39cc4e19196e2a7894917ae3abf8e738eab3cf7400b2fef54901a2488b179dcad"
    "1856bcaa77063bc9810fbcfb3fd90046be", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 24072U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_Llamosi2016_info.c) */
