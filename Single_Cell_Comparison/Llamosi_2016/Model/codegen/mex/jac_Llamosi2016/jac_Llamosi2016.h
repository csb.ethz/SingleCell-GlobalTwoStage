/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016.h
 *
 * Code generation for function 'jac_Llamosi2016'
 *
 */

#ifndef JAC_LLAMOSI2016_H
#define JAC_LLAMOSI2016_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "jac_Llamosi2016_types.h"

/* Function Declarations */
extern void jac_Llamosi2016(real_T varargin_1, const real_T varargin_2[3], const
  real_T varargin_3[4], real_T Jfx[9], real_T Jgx[9], real_T Jfp[12], real_T
  Jgp[12]);

#endif

/* End of code generation (jac_Llamosi2016.h) */
