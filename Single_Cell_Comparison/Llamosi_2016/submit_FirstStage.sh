#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N FirstStage_LlamosiModel
#$ -pe openmpi144 20
#$ -o ./FirstStage-LM.out
#$ -e ./FirstStage-LM.err
#$ -cwd
matlab -nosplash -r 'FirstStage_odeSD; exit'

