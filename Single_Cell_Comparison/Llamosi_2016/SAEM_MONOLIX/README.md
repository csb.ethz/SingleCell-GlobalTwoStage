This folder contains scripts to run SAEM for the osmo-schock model in the #!@Manuscript!!@

To execute the SAEM, look at the bash script ``run_SAEM.sh``. 

The data is Di_monolix_dnan.txt (generated from R)

The mlxtran file is Llamosi_2016_full.mlxtran which is the format used in MONOLIX. The txt model is Llamosi_model.txt.

The predictions are plotted using the script ``plot_SAEMpreds.m``. 

    sh run_SAEM.sh 
runs the estimation (user may have to change the the directory to the model based on where it is run from)

Requires MONOLIX2016 version or higher (Note for other versions of SAEM, the command may have to be modified)