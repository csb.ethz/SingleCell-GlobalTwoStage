-----------------------------------------------------------------------------------
# How to reproduce the results:
-----------------------------------------------------------------------------------
Firstly, make sure that the toolboxes ODESD OR AMICI, and NLOPT are correctly installed:
1. ODESD: https://git.bsse.ethz.ch/csb/odeSD  or 
2. AMICI: https://github.com/ICB-DCM/AMICI
3. NLOPT: https://nlopt.readthedocs.io/en/latest/ (STABLE RELEASE USED)

Then proceed by building the mex solver for the ODE model. The relevant script 
is in the folder ``Model``. This already contains the compiled model for LINUX environment, 
compiled using latest gcc compiler compatible with MATLAB2016b. 
The data used is provided in .csv format and is used for estimation. 
A conversion script from the original data (.mat, from http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004706)
to .csv is provided as an R script. 

The contents of the Model and Data folder are summarised below:

### Model: 	
#### Models in the folder: 

1. ``Xmodel.txt``: text form of the 'X' model, which can be compiled directly through ODESD using
	 			 conversion scripts. 
    * sym_Llamosi_model (model compiled by AMICI)
    * Llamosi2016: model without second order forward sensitivities
    * Llamosi2016_o2: model with second order forward sensitivities
				
2.	``var_fun.m`` : Variance model/ noise model used: 

			   INPUT==> noise parameters needed to define the noise mode, and the 
			   		  simulated trajectories
			   OUTPUT==> Returns the noise model (std. deviation model).

			   	
### Model related scripts:
1. ``ConvertModel.m`` : converts the text model to files needed to run the model by ODESD.
    * Requires libSBML, IQMTOOLS, to ensure conversion of the txt model. 
    * Using matlab/ .c models you only require odeSD.    
                    Please check ODESD documentation, in case of errors. mex files
                    for execution is in the respective model folder. 
 
2. Simulation scripts:
	* ``Run_Xmodel.m`` runs the model named 'Xmodel' using odeSD
	
				INPUT==> time (t), parameters (para), int_opts (integrator options)
				OUTPUT==> simulated trajectory f_t
	* `Run_Xmodel_wosens.m`` runs the model named 'Xmodel' without integrating sensitivities (odeSD)
				
                INPUT==> time (t), parameters (para), int_opts (integrator options)
				OUTPUT==> simulated trajectory f_t
	* ``RunLlamosi2016cvodes``: Runs the Llamosi model with CVODES (not used in manuscript)
	
	


### Data: 	 
Contains data used for estimation and scripts needed to process it

1. PlosCompDat.mat: Data from Llamosi's paper
			(http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004706).

2. Di_monolix_full.csv: Dataset used by us for estimation, in a table format
								   also used by Monolix. 

3. make_monolix_data.R: 
				Description: This scripts converts the data in PlosCompData to .csv file which 
							 can also be used by MONOLIX.

### Running the GTS:
Then, proceed by running the script run_GTS (sh run_GTS) if using an SGE cluster. 
						 run_GTS_desktop (sh run_GTS_desktop) if using a desktop. 

## NOTE
Modify the submits script according to the computer used.  To run it locally, the 
relevant files to modify if needed are: 
1. FirstStage.m, and SecondStage.m  to do the two stages of GTS
2. Profile likelihood of the noise parameters calculated in the folder profile_likelihood
3. The results are stored in Pubs_Results
4. Plot_prediction simulates the data from the estimated model and results are stored in 
Pubs_Results/plot_data


!!!NOTE!!!! Please modify lines related to data input, and storing if required. 
Make sure to change the paths of the installed toolboxes and the .mat files that are read 
in in the second stage. 
Export the NLOPT libraries correctly. 

-----------------------------------------------------------------------------------
# Folders and their Contents
-----------------------------------------------------------------------------------
1. Auxilliary: Contains help functions/ assisting functions
    * ``get_stableinverse.m``: 
					Description: Function that gets the inverse of a matrix. First Cholesky is tried. 
					If that fails, and the condition number is zero, then a small number 
					is added to the diagonal of the matrix. Then, the 'inv', based 
					on LU decomposition is used.  A warning is issued if Cholesky decomposition fails. 
					INPUT==> Matrix to invert
					OUTPUT==>Inverted Matrix, addition error indicator if Cholesky failed. 
	* ``getgradient_odesd.m``:
						Gets sensitivity of the ODE model and calculates the gradients of the response
 						with respect to the kinetic parameters. Note we are not caring about the parameters
 						that are assumed fixed. Then the sensitivity is used to get the FIM/ HESSIAN. 
  						y=exp(-gp*tau)*f_t_tau, then based on chain rule
 						dy_dp=exp(-gp*tau)*df_dp+d{exp(gp*tau)}*f
   						or use gen_Sens_y (in Model folder) to symbolically check. 

   						Tools needed: 
        						 a) ODE SOLVER : ODESD

  						INPUT==> t: Time values in experiment,
		  						p0: kinetic parameters
 		  						ehat: Noise parameters, theta in the model
								d: data
 								sigma2: sigma^2
								int_opts: Options passed to integrator, structure.

  						OUTPUT==> DYDP: the gradients wrt parameters
            					d2ydp2: second order gradients
			 					noisefuns:	the 'g' in manuscript
								 C_inv:		the FIM
           						 Hessian: The Hessin using second order sensitivities.
            					f_t: observation 
            					gradient_states: dxdp (sensitivities of the state 2 wrt
            					parameters)
	* ``get_secondordgradient.m``  (NOT used in manuscript)
				     Description: Gets sensitivity of the model output and calculates the 
					gradients of the response with respect to the kinetic parameters. 
					Then the sensitivity is used to get the FIM (C_inv) AND HESSIAN (C_INV_HESSIAN). 
					
					Tools needed: 
        						 a) ODE SOLVER : CVODES/ AMICI
        						 
				  INPUT==> t: Time values in experiment,
						   para: kinetic parameters
						ydata: data
 						noise_params: additive and multiplicative constant

   						
				OUTPUT==> f_t: observation 
							dydp, dy2dpdq: first and second order sensitivies of observation wrt parameters
							us: input state
							 H1, H2: C_inv/ C_inv_hessian
							residuals: residuals
							dx2dpdq: second order sensitivies of states wrt parameters
					
					
2. FirstStage_objs: Objective functions used in First stage of the pipeline: GLS-Pool
	* ``OLS.m``: Standard OLS (Ordinary Least Squares) method
	
			  	INPUT==>  t: Time values in experiment,
						kinetic_params: log kinetic parameters
						noise_params: Noise parameters, theta in the model
						d: data
						int_opts: Options passed to integrator, structure.
	           			model_fun: function handler to the ODE model

			 	OUTPUT==> res: The objective function ordinary least squares 
			 				(squared residuals)
	*  ``WOLS.m``: Does weighted OLS
	
				INPUT==> t: Time values in experiment,
			  			 kinetic_params: log kinetic parameters
	 		    		 weights: Weights calculated based on theta_hat & last estimates
				           			of kinetic_params using var_fun (g in manuscript)
						d: data
	           			int_opts: Options passed to integrator, structure.
						model_fun: function handler to the ODE model

			  	OUTPUT==> res: The objective function weighted ordinary least squares. 
	* ``get_AR_gm``: Gets the pooled AR (absolute residuals) likelihood. Implementation is based on 
						 https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
  						 Please see the  appendix of the paper

				 INPUT==> sim_struct: structure containing the simulated data for all
                						cells at their estimated kinetic parameters from
                       					last iteration
           			   	noise_params: Noise parameters, theta in the model
						data: data.Y
           				indexes: cell IDs (so far only those with all data)


				  OUTPUT==> AR: objective function (pooled AR, in log)
	*  ``get_PL_gm``: 	Gets the pooled PL (pseudolikelihood) likelihood. Implementation is based on 
							https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
						 	Please see the  appendix of the paper

				  INPUT==> sim_struct:  structure containing the simulated data for all
                       					cells at their estimated kinetic parameters from
                       					last iteration
           					noise_params: Noise parameters, theta in the model
							data: data.Y
           					indexes: cell IDs 
	* ``WLS_gm``: Used by get_AR_gm and get_PL_gm.m. Generates the things needed to 
		compute the AR/ PL objective functions
   
 			 	INPUT==> 	f_t: simulated response of the cell
 		   				noise_params: Noise parameters, theta in the model
		   				d: data

  				OUTPUT==>   res: residuals
			  			h:  noise model per cell. 
 			  			gm: log geometric mean noise function of the cell

4. SecondStage_objs: Objective functions used in the SecondStage
	*  ``LGTS_EM.m``: Second Stage EM algorithm based on equations 5.12, 5.13 and 5.14 Chapter 5 of
						 Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
						 1996 Reprint edition, Davidian and Giltinan .

 				INPUT==> C_inv: FIM for all the estimates, 3D array
			 			betahat0: initial parameters of the mean
			 			Dhat0: initial parameters of the covariance
			 			beta_ihat: individual estimates from the first stage,
 			 			conv: structure containing information regarding convergence crietria, 
								tolerance, maximum iteration
				OUTPUT==> Result: Structure with following entries: 
			              	PopCovar: (D) Cov. matrix 
               			  	eBayesEsts: (beta_i) ebayes individual estimates
                          	PopMean: (betahat) mean parameter estimate
                          	Mean_convergence: (betadiffs) convergence of parameter vector: differences
               			  	Var_convergence: (diffs) convergence of D: differences
               			  	Var_allvalues: (allDs) Values of entries of D (covariance) at each
                               iteration
						    Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
                               iteration		
		
				
4. Model/ Data: Contains things needed to run the model. See last section on reproducing the 
		  study. 
		  
5. ``profile_likelihood``: Contains scripts to get the profile likelihood of the noise parameters
	* ``get_proflik.m``: main function to run
			Calculates the confidence intervals for the noise parameters
			using profile likelihood. The input file used is the results from the first stage and data.
			The result .mat file contains the information needed to draw the profile likelihoods. 
			A figure is generated. 

              Tools needed: 
                    a) ODE SOLVER
                    b) Model
              Funtions, objective functions used: 
            		a) calc_AR: the objective function used, if AR pool was used to get the theta_hats, 
            		 else use calc_PL
            		b) Model integration
              Lines that have to modified:
                  % a) likleihood calculation (calc_AR/ calc_PL)
            		b) folder containing results of the first stage

6. SAEM_MONOLIX: Contains things to run SAEM in MONOLIX. Look inside for implementation details. 	
7. NAIVE_STS: Contains scripts to run the 'Naive' analysis as done by the original publication by
			  Llamosi et al , 2016. 

-----------------------------------------------------------------------------------
Main scripts
-----------------------------------------------------------------------------------
1. ``FirstStage.m``:   Implements the First stage for the analysis of the data from Llamosi et al, 2016. Reads in 
                      the data used in Data/ and saves the results in output folder.
                      If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
                          qsub submit_FirstStage.sh
        The resulting mat files store a lot of information: 

  			The estimates, function values, return code, data and model simulation for each sell in a structure: 'GLS_params'
   			Information on the convergence of - parameters: 'diff_noise','diff_p1','diff_p2','diff_p3'
  												- function value of WOLS of each cell at each iteration: 'diff_fval'
  												- Measure of convergence at each iterations (relative difference between estimates): 'diffs'
		 	CPU time : 'cptime'  
  			noise parameters:'theta_hat', 'sigma2'
  			Individual FIMs: a 3-D array 'C_inv' 
  			'betai_hat' an array the individual parameter estimates
  		
          Tools needed: 
                a) ODE SOLVER
                b) Optimizer: NLOPT
                
          Funtions, objective functions used: 
                a) OLS estimates
                b) WLS
                c) POOLED GLS 
                d) Model integration, and evaluating the error model
                e) Auxilliary functions to calculate the fisher information matrix.      
          
          !!! Lines that have to modified if needed:
                a) the paths to ODE integrator, and Optimizer. 
                b) Output directory
                c) parallellization options
2.  ``SecondStage.m``:  Implements the Second stage for the analysis of the data from Llamosi et al, 2016. 
			If using an SGE cluster, this can be run using the accompanying .sh script. The resulting
			mat file contains following information: 

				  'cpout': CPU TIME taken
	  			  'Pop_ests':  Result structure containing:
                   				PopCovar: (D) Cov. matrix 
                   				eBayesEsts: (beta_i) ebayes individual estimates
                   				PopMean: (betahat) mean parameter estimate
                   				Mean_convergence: (betadiffs) convergence of parameter vector: differences
                   				Var_convergence: (diffs) convergence of D: differences
                   				Var_allvalues: (allDs) Values of entries of D (covariance) at each
                                   iteration
                   				Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
                                   iteration   
                   				Asymcovs: Asym. covariance of betahat
      			'theta_hat','sigma2', 'C_inv': noise parameters and the inidividual uncertainties
	  			'beta_i': initial individual estimates
      			'betahat, Dhat': Final population mean and covarariance of the parameters 
 
              	Funtions, objective functions used: 
                     a) LGTS_EM
                     b) Auxilliary functions to calculate matrix inverse. 
            
        !!!!   Lines that have to modified:
            a) Input file name, result of the first stage
            b) Output directory
        
    If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
       qsub submit_SecondStage.sh
3. ``Plot_prediction.m``:  Generates the predictions for the results from GTS. And saves it as an array, 
  where each coloumn is the time and the rows are independent simulated trajectories. 

          Tools needed: 
                a) ODE SOLVER
          Functions used: 
                c) Model integration, and evaluating the error model  
          !!!Lines that can be modified if needed
              % a) the paths to ODE integrator, and model
                b) Output directory: plots_data (Default)
        		c) Path to GTS results: Pubs_Results (Default)
        		d) Number of simulations: 10000 (Default)
    	  
-------------------------------------------------------------------------------------
# bash scripts
-------------------------------------------------------------------------------------
These scripts help to execute the code in the BSSE grid. and can be submited using ``qsub``

	submit_FirstStage : to run First Stage 	
	submit_SecondStage: to run the Second Stage	
	submit_plot		  : to run Second Stage

-------------------------------------------------------------------------------------
# Pipeline Script
-------------------------------------------------------------------------------------	
GTS (First+ Second Stage + plotting prediction) can be executed in a pipeline fashion using:

    sh run_GTS

Some .err and .out will be created and will contain any execution information.


To run the GTS in a desktop:

    sh run_GTS_desktop 

(Only difference between the previous script is 
no jobs are submitted, laubches a MATLAB desktop and runs things there.)
 
