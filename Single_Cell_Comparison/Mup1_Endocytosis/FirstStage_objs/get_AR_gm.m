%%%------------------------------------------------------------------------
%
%   function get_AR_gm.m
%	Gets the pooled AR (absolute residuals) likelihood. Implementation is based on 
%	"Some Simple Methods for Estimating Intraindividual Variability in Nonlinear Mixed
%	Effects Models"
%	Author(s): Marie Davidian and David M. Giltinan
%	Source: Biometrics, Vol. 49, No. 1 (Mar., 1993), pp. 59-73
%   https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
%  	Please see the  appendix of the paper
%   
%
%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> Final_params: Structure containing simulated data at optimal betahat_is
%			e_par: Error parameters passed from the optimiser. 

%  OUTPUT==> AR: objective function (pooled AR, in log)
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------


function AR=get_AR_gm(Final_params,e_par)
AR_i=[];
%indexes=length(Final_params); % total cells 
for_pl=struct();
e_par=exp(e_par);

%for i = 1: length(indexes)
    availtime = ~isnan(Final_params.ydata);
% Get the observables for each cell, kk is input
	for_pl.res=Final_params.ydata(availtime)-Final_params.observables(availtime); % residuals of each simulated point from data, all cells
	for_pl.gmodel=e_par(1)+Final_params.observables(availtime).*e_par(2);         % Noise model at each simulated point for all cells 
%end

	g_dot=log(geomean(cell2mat({for_pl.gmodel}')));% geometric mean of all N noise models
    
%for i = 1: length(indexes)
    AR_i=sum(sum((abs(for_pl.res).*exp(g_dot)./for_pl.gmodel)));
%end

AR=log(sum(AR_i));
