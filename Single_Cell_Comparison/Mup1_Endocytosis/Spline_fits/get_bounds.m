% Getting the bounds and plotting the data from the spline fits.  

load('Spline_paramsall.mat')

df_fit=cat(1,Final_params.spline_obs);
df=cat(1,Final_params.resids);


% Plot all the fits
figure()
for jj=5%1:6
subplot(2,3,jj)
scatter(df_fit(:,jj),abs(df(:,jj)),'filled','MarkerFaceAlpha',3/10)
xlabel('Spline fit')
ylabel('Y-spline fit')
end

% The distributions are skewed... get the estimates of theta1s
% See if there is a linear relationship between the residuals 
% and the values. 
all_pars=struct();
parfor k=1:6
 theta1s=zeros(1,length(Final_params));
for i=1:length(Final_params)
 fited=fitlm(Final_params(i).spline_obs(:,k),(Final_params(i).resids(:,k)));
 theta1s(i)=fited.Coefficients.Estimate(2);
 
end
 all_pars(k).theta1s=theta1s;
end

% Get the pooled residuals to set as theta0s. 
All_resids=cat(1,Final_params.resids);
All_dofs=cat(1,Final_params.dfes);
All_rmses=cat(1,Final_params.rmse);
theta0s_bounds=quantile(All_rmses,[0.01,0.5,0.99])';

% Get the bounds of theta1s. 
all_theta1s=cat(1,all_pars.theta1s)';
theta1s_bounds=quantile((all_theta1s),[0.01,0.5,0.99])';
save('noise_bounds.mat', 'theta0s_bounds','theta1s_bounds')

% fit value
save('fits.mat','df_fit','df')


