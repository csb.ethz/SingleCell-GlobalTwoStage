% Plot the predictions from the population estimates. and calculate the
% Jaccard index. The input is the data and the results. The figures, and 
% the jaccard indices are plotted. 
% Needs manual modification of the PATHS> 
% Lekshmi D
% January 2018

clear all
% Modify paths: 
addpath(genpath('./'))

addpath(genpath('./Model')) 
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('./Auxilliary')) 

% Load the data
load('./Data/Indcell_data.mat');
df=data;
[ind,inds]=sort(df.Time);
data=data(inds,:);

%% Comment relevant parts based on the results loaded
% Load the SAEM result
%load('SAEM_FILES/Mup1_GTS/Mup1_GTS.mat')
%betahat=struct2array(betahat);
%dirname='./SAEM_FILES/Mup1_GTS/SAEM_predictions';

% GTS results
load('./Pub_Results/NLMD50_5.mat')
dirname='PREDICTIONS/NLMD_PREDICTIONS_505';
err=[all_theta0s,all_theta1s];


%% Simulate parameters and model
gts_preds=struct;
for i=1:10000
rng(i)
time=linspace(-10,78,200);
%params_k=[randsample(1:length(data.Vol_cell),1),randsample(1:length(data.Vol_mem),1)];
params_sim=mvnrnd(betahat,Dhat);

int_opts.atol=1e-6;
int_opts.rtol=1e-3;
[f,t,simulated]=Endo_eq(params_sim',time,[],[],int_opts);
%[f,t,simulated]=Endo_eq(GLS_params(2).params',time,[],[],int_opts);
if ~isempty(simulated)
gts_preds(i).simulated=simulated;
gts_preds(i).VC=simulated(:,1);
gts_preds(i).VM=simulated(:,2);
gts_preds(i).IC=simulated(:,3);
gts_preds(i).IM=simulated(:,4);
gts_preds(i).IS=simulated(:,5);
gts_preds(i).ICP=simulated(:,6);
end
end

%%
mkdir(dirname)
%mkdir('SAEMimages_95')
VC=cat(2,gts_preds.VC)';
VM=cat(2,gts_preds.VM)';
IC=cat(2,gts_preds.IC)';
IM=cat(2,gts_preds.IM)';
ICP=cat(2,gts_preds.ICP)';
IS=cat(2,gts_preds.IS)';
all_eas=err(:,1)';
all_ebs=err(:,2)';
%constantnoise
%all_ebs=zeros(size([err(:,2)]));

All_simulated=zeros(size(VC,1),size(VC,2),6);
All_simulated(:,:,1)=VC+normrnd(0,1,size(VC)).*(all_eas(1));
All_simulated(:,:,2)=VM+normrnd(0,1,size(VM)).*(all_eas(2));
All_simulated(:,:,3)=IC+normrnd(0,1,size(IC)).*(all_eas(3)+IC.*all_ebs(3));
All_simulated(:,:,4)=IM+normrnd(0,1,size(IM)).*(all_eas(4)+IM.*all_ebs(4));
All_simulated(:,:,5)=IS+normrnd(0,1,size(IS)).*(all_eas(5)+IS.*all_ebs(5));
All_simulated(:,:,6)=ICP+normrnd(0,1,size(ICP)).*(all_eas(6)+ICP.*all_ebs(6));
figure()
data=sortrows(data, 'Time');
plot_dat=[data.Vol_cell,data.Vol_mem,data.GFP_cell,data.GFP_mem,data.GFP_spot,data.GFP_cp];
titles={'Cell volume:V_C','Membrane volume:V_M','GFP Cytoplasm Intensity: I_C','GFP Membrane Intensity:I_M','Spot Intensity:I_S','Cytoplasmic Intensity:I_{CP}'};
Time_dat=unique(round(data.Time));

% Save data quantiles
dat_5=[];
dat_05=[];
dat_95=[];
dat_025=[];
dat_975=[];

% savr simulation quantiles
All_quan_95=[];
All_quan_05=[];
All_quan_50=[];
All_quan_975=[];
All_quan_025=[];
for j=1:6
duf=reshape(plot_dat(:,j),1907,45);

dm100=quantile(duf,0.975);
dm00=quantile(duf,0.025);
dm50=quantile(duf,0.5);

m100= quantile(All_simulated(:,:,j),0.975);
m00= quantile(All_simulated(:,:,j),0.025);
m50=quantile(All_simulated(:,:,j),0.5);

% Save all the Dhat and betahat
All_quan_99(1,:,j)=quantile(All_simulated(:,:,j),0.99);
All_quan_01(1,:,j)=quantile(All_simulated(:,:,j),0.01);
All_quan_975(1,:,j)=quantile(All_simulated(:,:,j),0.975);
All_quan_025(1,:,j)=quantile(All_simulated(:,:,j),0.025);
All_quan_50(1,:,j)=quantile(All_simulated(:,:,j),0.5);

% Dtata quantiles
dat_5(:,j)=quantile(duf,0.5);
dat_01(:,j)=quantile(duf,0.01);
dat_99(:,j)=quantile(duf,0.99);
dat_025(:,j)=quantile(duf,0.025);
dat_975(:,j)=quantile(duf,0.975);

% Plot
h3=fill([Time_dat' fliplr(Time_dat')]./60,[dm00 fliplr(dm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([m00,dm00]),max([dm100,m100])]);
%ylim([min([dm00,-0.5])-2*[min([dm00,0.5])],max([dm100])+0.5*[max([dm00])]]);
set(gca,'ytick',[],'xtick',[])
set(gcf, 'Units', 'normalized', 'Position', [0, 0,1,1], 'PaperUnits', 'Inches', 'PaperSize', [7.25, 9.125])
saveas(gcf,sprintf('%s/dat_%d.png',dirname,j))
close

h3=fill([time fliplr(time)]./60,[m00 fliplr(m100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
ylim([min([m00,dm00]),max([dm100,m100])]);
%ylim([min([dm00,-0.5])-2*[min([dm00,0.5])],max([dm100])+0.5*[max([dm00])]]);

set(gca,'ytick',[],'xtick',[])
set(gcf, 'Units', 'normalized', 'Position', [0, 0,1,1], 'PaperUnits', 'Inches', 'PaperSize', [7.25, 9.125])
saveas(gcf,sprintf('%s/pred_%d.png',dirname,j))
close

end


%% get Jaccard Index
tosave=[];

for j=1:6
    
% Open
dat=imread(sprintf('%s/dat_%d.png',dirname,j));
pred=imread(sprintf('%s/pred_%d.png',dirname,j));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));

% Get Jaccard index.
intersectImg = dat & pred;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[ j, Jac_index]);
end


JI_GTS=tosave;
quan_99     =All_quan_99;
quan_01   =All_quan_01;
quan_975   =All_quan_975;
quan_025 =All_quan_025;
quan_50 =All_quan_50;

sim_time=linspace(-10,78,200);
save(sprintf('%s/data_image_95.mat',dirname),'titles','JI_GTS','quan_50','quan_025','quan_975','quan_01','quan_99','dat_99','dat_01','dat_5','dat_025','dat_975','Time_dat', 'All_simulated','plot_dat','sim_time')

    