#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N SecondStage_Mup1
#$ -o ./SecondStage-LM.out
#$ -e ./SecondStage-LM.err
#$ -cwd
matlab -nosplash  -r 'SecondStage; exit'
