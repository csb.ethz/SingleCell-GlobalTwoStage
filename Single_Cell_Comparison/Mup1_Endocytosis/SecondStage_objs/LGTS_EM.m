function [Result]=LGTS_EM(C_inv,betahat0,Dhat0,beta_ihat,conv)
%%%------------------------------------------------------------------------
%
%   function LGTS_EM.m
%	Second Stage EM algorithm based on equations 5.12, 5.13 and 5.14 Chapter 5 of
%   Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%   1996 Reprint edition, Davidian and Giltinan .
%
%
%  INPUT==>  C_inv: FIM for all the estimates, 3D array
%			 betahat0: initial parameters of the mean
%			 Dhat0: initial parameters of the covariance
%			 beta_ihat: individual estimates from the first stage,
% 			 conv: structure containing information regarding convergence crietria, 
%					tolerance, maximum iteration
%
%  OUTPUT==> Result: Structure with following entries: 
%               PopCovar: (D) Cov. matrix 
%               eBayesEsts: (beta_i) ebayes individual estimates
%               PopMean: (betahat) mean parameter estimate
%               Mean_convergence: (betadiffs) convergence of parameter vector: differences
%               Var_convergence: (diffs) convergence of D: differences
%               Var_allvalues: (allDs) Values of entries of D (covariance) at each
%                               iteration
%               Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
%                               iteration
 

%   Lekshmi Dharmarajan, July 2017
%   Checked January 2018
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

%% Initialize
betahat=betahat0;
D = Dhat0;

% Convergence criteria
diffs=[];     % stores the differences in successive iteration in D
betadiffs=[]; % stores the differences in successive iteration in betahat
allbs=[];	  % stores the values of betahats at each iteration
allDs=[];	  % stores the values of D estimates at each iteration


% Initialize
diffbeta=betahat; 
diffD=D;		 
niter=0; 		 % iteration counter
allDs=cat(1,allDs,reshape(D,1,numel(D)));
allbs=cat(1,allbs,betahat);

% Convergence criteria
niter=0;
maxiter=conv.maxiter;
tol=conv.tol;

% convergence estimate
while niter<maxiter&&(max(max(diffbeta))>tol||max(max(diffD))>tol) % TODO: Re-write it. 

%% E-step: get expected individual estimates 
beta_i=zeros(size(beta_ihat));
for  i=1:length(beta_ihat)
D_inv=get_stableinverse(D);
beta_i(i,:)=get_stableinverse(C_inv(:,:,i)+D_inv)*(C_inv(:,:,i)*beta_ihat(i,:)'+D_inv*betahat'); % Equation 5.12
end
 
%% M-step
wi_t1=zeros(size(D)); %term 1 of Weight below 5.13
betadiff=zeros(size(betahat));

% estimate betahat
for i=1:length(beta_i)
wi_t1=wi_t1+D_inv; %sum over all D^-1
end
Wi=get_stableinverse(wi_t1)*D_inv; % 5.13
betahat0=betahat; %at iteration c
D0=D;
beta_i0=beta_i;
betahat=zeros(size(betahat));
for  i=1:length(beta_i)
betahat=betahat+(Wi*beta_i(i,:)')';%sigma Wi_k*beta_i_k
end

% estimate D, given betahat
D_t1=zeros(size(D));
D_t2=0;
for i=1:length(beta_i)
D_t1=D_t1+get_stableinverse(C_inv(:,:,i)+D_inv);  %term 1 from 5.15
betadiff=(betahat-beta_i(i,:));
D_t2=D_t2+betadiff'*betadiff; %term 2 from 5.15
betadiff=(betahat-beta_i(i,:));
end
D_t2=D_t2./size(beta_i,1);
D=D_t1./size(beta_i,1)+D_t2;

% See if things have converged
% Based on Absolute differences
diffD=abs(D0-D);
diffbeta=abs(betahat0-betahat);

% Update information
diffs=cat(1,diffs,max(max(diffD)));
betadiffs=cat(1,betadiffs,max(max(diffbeta)));
allDs=cat(1,allDs,reshape(D,1,numel(D)));
allbs=cat(1,allbs,betahat);
niter=niter+1;

% If for some reason, some inversion failed, then stop the algorithm! 
% This rarely occurs. 
if any(isnan(D))
    D=D0;
    betahat=betahat0;
    beta_i=beta_i0;
    niter=100000;
    disp('WARNING: D has NaN values!!')
end

% Aggregate results into a structure to return it to the main file.
Result=struct;
Result.PopCovar=D;
Result.eBayesEsts=beta_i;
Result.PopMean=betahat;
Result.niter=niter;
Result.Mean_convergence=betadiffs;
Result.Var_convergence=diffs;
Result.Var_allvalues=allDs;
Result.Mean_allvalues=allbs;
Result.conv_setting=conv;

end

