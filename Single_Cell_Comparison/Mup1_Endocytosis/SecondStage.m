function []=SecondStage()
%%%------------------------------------------------------------------------
%
%   function SecondStage.m
% 	Implements the Second stage for the analysis of the data from Llamosi et al, 2016. 
% 	If using an SGE cluster, this can be run using the accompanying .sh script. The resulting
%   mat file contains following information: 
	%  'cpout': CPU TIME taken
	%  'Pop_ests':  Result structure containing:
    %               PopCovar: (D) Cov. matrix 
    %               eBayesEsts: (beta_i) ebayes individual estimates
    %               PopMean: (betahat) mean parameter estimate
    %               Mean_convergence: (betadiffs) convergence of parameter vector: differences
    %               Var_convergence: (diffs) convergence of D: differences
    %               Var_allvalues: (allDs) Values of entries of D (covariance) at each
    %                               iteration
    %               Mean_allvalues: (allbs)Values of entries of betahat (mean) at each
    %                               iteration   
    %               Asymcovs: Asym. covariance of betahat
    %  'theta_hat','sigma2', 'C_inv': noise parameters and the inidividual FIMs
	%  'beta_i': initial individual estimates
    %  'betahat, Dhat': Final population mean and covarariance of the parameters 
 
%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%       qsub submit_SecondStage.sh

%  	Funtions, objective functions used: 
        % a) LGTS_EM
        % b) Auxilliary functions to calculate matrix inverse.     
%   
%   Lines that have to modified:
%       % a) input file name, result of the first stage
        % b) Output directory

%   Lekshmi Dharmarajan, July 2017
%   Modified: Jan 2018
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------
	addpath(genpath('Auxilliary'))   % path to helper functions
	addpath(genpath('SecondStage_objs')) % path to objective functions used

	%%%%%%%%%%%% Tweaking prameters 
	outdir='Pub_Results';										% Folder to store the results (USER MUST MODIFY)
    inpfile='./Pub_Results/FirstStageNLMD_iter5.mat';
	cp1=cputime();
	load(inpfile)

	% Initialize
	conv=struct();                                                                                          % Convergence criteria
        conv.maxiter=5000; % maximum iteration
        conv.tol=1e-3;     % absolute tolerance


    cell_IDS=cat(1,GLS_params.cellID);
    params=cat(1,GLS_params.params);
	beta_i=params;
    
    %Inv_FIM=zeros(length(GLS_params(1).params),length(GLS_params(1).params),length(GLS_params));
    FIM=zeros(length(GLS_params(1).params),length(GLS_params(1).params),length(GLS_params));
    for i=1:length(GLS_params)
    % Simulate 
    FIM(:,:,i)=GLS_params(i).FIM;
    %[ Inv_FIM(:,:,i)] =get_stableinverse(FIM(:,:,i));
    end
     
	% Initialising the EM with population estimates. 
	beta_nts=median(beta_i); 
	D_nts=cov(beta_i);
    C_inv=FIM;

	% The function that does the estimation.
	ll_gts=@(betahat0,Dhat0) LGTS_EM(C_inv,betahat0,Dhat0,beta_i,conv);
	Pop_ests=ll_gts(beta_nts,D_nts);
   	betahat=Pop_ests.PopMean;
    	Dhat=Pop_ests.PopCovar;
	betahati=Pop_ests.eBayesEsts;
	sprintf('Second stage converged in %d iterations', Pop_ests.niter)

	% Asym. Covariance matrix: Again here, sometimes inversion issues can happen and things may fail. 
	% NaN values usually mean that the matrix is too stiff. One must look closely why this happened. 
	% One can ignore the cells having inversion issues and calculate the asym. covariance matrix. 
	disp('Getting uncertainities of the mean estimates')

    
	asymcovs=zeros(length(betahat),length(betahat));
    excluded_cells=[];
	for i=1:length(Pop_ests.eBayesEsts)
    % Exclude the cells which had inversion problems. 
    [Asymcov_cell,err]=get_stableinverse(C_inv(:,:,i));
        
    % check if the inversion below will be successful
    % A lot of cells failed cholesky (~500). In general,this is stringent
    % What matters is that the inversion below works. 
        if(any(any(isinf(get_stableinverse(Dhat+Asymcov_cell)))))
        % if err~=0
        excluded_cells=cat(1,excluded_cells,i);
        else
        asymcovs=asymcovs+get_stableinverse(Dhat+Asymcov_cell);
        end

	end
	Pop_ests.Asymcov=get_stableinverse(asymcovs);

    cpout=cputime()-cp1;
 
	% Save results 												%(USER MUST MODIFY)
	mkdir(outdir) 
	save(sprintf('%s/NLMD50_5.mat',outdir),'cpout','excluded_cells','all_theta0s','all_theta1s','cpout','cell_IDS','D_nts','beta_nts','beta_i','Dhat','betahat','theta_hat','C_inv','betahati','Pop_ests')
end
