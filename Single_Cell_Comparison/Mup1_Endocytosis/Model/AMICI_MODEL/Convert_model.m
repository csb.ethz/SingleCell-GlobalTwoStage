%% Converts Mup1 model to AMICI format
% make sure all the needed compilers/ and path to CBLAS 
% is accessible. 
% If not: do
%export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/lib64/atlas/
%export LIBRARY_PATH=${LIBRARY_PATH}:/usr/lib64/atlas/

addpath(genpath('~/Repos/AMICI')) % path to AMICI
mkdir('Endocytosis_model')
amiwrap('Endo_model','model_endocytosis','Endocytosis_model')