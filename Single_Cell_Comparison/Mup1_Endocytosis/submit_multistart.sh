#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Rerun-Checks
##$ -o /links/grid/scratch/dlekshmi/
##$ -e /links/grid/scratch/dlekshmi/
#$ -cwd
##$ -l h_rt=00:20:00
#$ -l h_vmem=8g

for i in $(seq $SGE_TASK_STEPSIZE);  do
 index=$(($SGE_TASK_ID+$i))
 echo "Submitted $index"
 matlab -nosplash  -r "MultiStart_WLS($(($SGE_TASK_ID+$i))-1); exit"
done

