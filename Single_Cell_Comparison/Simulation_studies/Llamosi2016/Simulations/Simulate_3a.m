%% Simulate the data for figure 3a. 
% Clean data, where every cell is measured accurately. 


clear all
addpath(genpath('~/Repos/odeSD/')); % Add path to model integrator
addpath(genpath('../../../Llamosi_2016/Model/'))

% Load the data 
cellnums=[50,150,250,350,500];


% has to be changed based on the result file name.  
result_directory='../../../Llamosi_2016/SAEM_MONOLIX/';
load(sprintf('%s/Llamosi2016_full_SAEMPreds.mat',result_directory));

% Default noise to be used in the cell
%e_hat(1)=sqrt(sigma2)*theta_hat(1);
%e_hat(2)=log(sqrt(sigma2)*exp(theta_hat(2)));%this can only be positive, 0.25 times 
e_hat(1)=theta_0;
e_hat(2)=log(theta_1);
tau=3.4;
e_hat=e_hat;
int_opts.abstol=1e-6;
int_opts.reltol=1e-3;

seeds=123456009;
for ii=1:5%length(cellnums)
  cellnum=cellnums(ii);  
for J=1:5
    
Model_prediction2=[];
TIME=[];
ID=[];
Y=[];
p=[];
for i=1:cellnum    
rng(seeds)
paras0=exp(mvnrnd(betahat,Dhat));

seeds=seeds+1;
[f_t]=Run_Llamosi2016(0:6:600,[paras0(1:2),paras0(3)],int_opts);

ID=cat(1,ID,repmat(i,length(f_t),1));
TIME=cat(1,TIME,[0:6:600]');
p=cat(1,p,paras0);
Y=cat(1,Y,[f_t'+(e_hat(1)+exp(e_hat(2))*f_t').*randn(size(f_t'))]);
Model_prediction2=cat(1,Model_prediction2,f_t');
end

Data=table(ID,Y,TIME);
% 
mkdir('Simulated_Data')
mkdir(sprintf('Simulated_Data/Data_Fig3a'))
mkdir(sprintf('Simulated_Data/Data_Fig3a/%d',cellnum))
save(sprintf('./Simulated_Data/Data_Fig3a/%d/Simulated_%d.mat',cellnum,J),'p','Data','Dhat','betahat','e_hat','seeds')%end
writetable(Data,sprintf('./Simulated_Data/Data_Fig3a/%d/Simulated%d.txt',cellnum,J))


end
end



