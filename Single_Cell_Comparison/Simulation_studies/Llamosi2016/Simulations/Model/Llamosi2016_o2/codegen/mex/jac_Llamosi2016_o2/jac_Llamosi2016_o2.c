/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_o2.c
 *
 * Code generation for function 'jac_Llamosi2016_o2'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016_o2.h"
#include "blas.h"

/* Function Definitions */
void jac_Llamosi2016_o2(const emlrtStack *sp, real_T varargin_1, const real_T
  varargin_2[12], const real_T varargin_3[12], const real_T varargin_4[4],
  real_T Jfx[144], real_T Jgx[144], real_T Jfp[48], real_T Jgp[48])
{
  real_T alpha1;
  real_T beta1;
  char_T TRANSB;
  char_T TRANSA;
  ptrdiff_t m_t;
  ptrdiff_t n_t;
  ptrdiff_t k_t;
  ptrdiff_t lda_t;
  ptrdiff_t ldb_t;
  ptrdiff_t ldc_t;
  (void)sp;
  (void)varargin_1;

  /*  odeSD */
  memset(&Jfx[0], 0, 144U * sizeof(real_T));
  Jfx[0] = -varargin_4[1];
  Jfx[1] = 0.947;
  Jfx[6] = -1.0;
  Jfx[13] = -varargin_4[2];
  Jfx[22] = -1.0;
  Jfx[24] = varargin_4[0];
  Jfx[26] = -0.9225;
  Jfx[27] = 1.0;
  Jfx[39] = -varargin_4[1];
  Jfx[40] = 0.947;
  Jfx[52] = -varargin_4[2];
  Jfx[63] = varargin_4[0];
  Jfx[65] = -0.9225;
  Jfx[78] = -varargin_4[1];
  Jfx[79] = 0.947;
  Jfx[91] = -varargin_4[2];
  Jfx[102] = varargin_4[0];
  Jfx[104] = -0.9225;
  Jfx[117] = -varargin_4[1];
  Jfx[118] = 0.947;
  Jfx[130] = -varargin_4[2];
  Jfx[141] = varargin_4[0];
  Jfx[143] = -0.9225;
  alpha1 = 1.0;
  beta1 = 0.0;
  TRANSB = 'N';
  TRANSA = 'N';
  memset(&Jgx[0], 0, 144U * sizeof(real_T));
  m_t = (ptrdiff_t)12;
  n_t = (ptrdiff_t)12;
  k_t = (ptrdiff_t)12;
  lda_t = (ptrdiff_t)12;
  ldb_t = (ptrdiff_t)12;
  ldc_t = (ptrdiff_t)12;
  dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &Jfx[0], &lda_t, &Jfx[0],
        &ldb_t, &beta1, &Jgx[0], &ldc_t);
  memset(&Jfp[0], 0, 48U * sizeof(real_T));
  Jfp[0] = varargin_2[2];
  Jfp[3] = varargin_2[5];
  Jfp[6] = varargin_2[8];
  Jfp[9] = varargin_2[11];
  Jfp[12] = -varargin_2[0];
  Jfp[15] = -varargin_2[3];
  Jfp[18] = -varargin_2[6];
  Jfp[21] = -varargin_2[9];
  Jfp[25] = -varargin_2[1];
  Jfp[28] = -varargin_2[4];
  Jfp[31] = -varargin_2[7];
  Jfp[34] = -varargin_2[10];
  alpha1 = 1.0;
  beta1 = 0.0;
  TRANSB = 'N';
  TRANSA = 'N';
  memset(&Jgp[0], 0, 48U * sizeof(real_T));
  m_t = (ptrdiff_t)12;
  n_t = (ptrdiff_t)4;
  k_t = (ptrdiff_t)12;
  lda_t = (ptrdiff_t)12;
  ldb_t = (ptrdiff_t)12;
  ldc_t = (ptrdiff_t)12;
  dgemm(&TRANSA, &TRANSB, &m_t, &n_t, &k_t, &alpha1, &Jfx[0], &lda_t, &Jfp[0],
        &ldb_t, &beta1, &Jgp[0], &ldc_t);
  Jgp[0] += varargin_3[2];
  Jgp[3] += varargin_3[5];
  Jgp[6] += varargin_3[8];
  Jgp[9] += varargin_3[11];
  Jgp[12] += -varargin_3[0];
  Jgp[15] += -varargin_3[3];
  Jgp[18] += -varargin_3[6];
  Jgp[21] += -varargin_3[9];
  Jgp[25] += -varargin_3[1];
  Jgp[28] += -varargin_3[4];
  Jgp[31] += -varargin_3[7];
  Jgp[34] += -varargin_3[10];
}

/* End of code generation (jac_Llamosi2016_o2.c) */
