/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_o2_api.c
 *
 * Code generation for function '_coder_Llamosi2016_o2_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016_o2.h"
#include "_coder_Llamosi2016_o2_api.h"
#include "Llamosi2016_o2_data.h"

/* Function Declarations */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *b_emlrt_marshallOut(const real_T u[144]);
static real_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *varargin_2, const char_T *identifier))[12];
static const mxArray *c_emlrt_marshallOut(const real_T u[48]);
static real_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[12];
static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *varargin_3, const char_T *identifier))[4];
static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *varargin_1,
  const char_T *identifier);
static const mxArray *emlrt_marshallOut(const real_T u[12]);
static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4];
static real_T g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[12];
static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4];

/* Function Definitions */
static real_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = g_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const real_T u[144])
{
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv2[2] = { 0, 0 };

  static const int32_T iv3[2] = { 12, 12 };

  y = NULL;
  m1 = emlrtCreateNumericArray(2, iv2, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m1, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m1, iv3, 2);
  emlrtAssign(&y, m1);
  return y;
}

static real_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *varargin_2, const char_T *identifier))[12]
{
  real_T (*y)[12];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(varargin_2), &thisId);
  emlrtDestroyArray(&varargin_2);
  return y;
}
  static const mxArray *c_emlrt_marshallOut(const real_T u[48])
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv4[2] = { 0, 0 };

  static const int32_T iv5[2] = { 12, 4 };

  y = NULL;
  m2 = emlrtCreateNumericArray(2, iv4, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m2, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m2, iv5, 2);
  emlrtAssign(&y, m2);
  return y;
}

static real_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[12]
{
  real_T (*y)[12];
  y = h_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *varargin_3, const char_T *identifier))[4]
{
  real_T (*y)[4];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(varargin_3), &thisId);
  emlrtDestroyArray(&varargin_3);
  return y;
}

static real_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *varargin_1,
  const char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(varargin_1), &thisId);
  emlrtDestroyArray(&varargin_1);
  return y;
}

static const mxArray *emlrt_marshallOut(const real_T u[12])
{
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv0[1] = { 0 };

  static const int32_T iv1[1] = { 12 };

  y = NULL;
  m0 = emlrtCreateNumericArray(1, iv0, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m0, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m0, iv1, 1);
  emlrtAssign(&y, m0);
  return y;
}

static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4]
{
  real_T (*y)[4];
  y = i_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[12]
{
  real_T (*ret)[12];
  static const int32_T dims[1] = { 12 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[12])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4]
{
  real_T (*ret)[4];
  static const int32_T dims[1] = { 4 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[4])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

void Llamosi2016_o2_api(const mxArray * const prhs[3], const mxArray *plhs[6])
{
  real_T (*f)[12];
  real_T (*g)[12];
  real_T (*Jfx)[144];
  real_T (*Jgx)[144];
  real_T (*Jfp)[48];
  real_T (*Jgp)[48];
  real_T varargin_1;
  real_T (*varargin_2)[12];
  real_T (*varargin_3)[4];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  f = (real_T (*)[12])mxMalloc(sizeof(real_T [12]));
  g = (real_T (*)[12])mxMalloc(sizeof(real_T [12]));
  Jfx = (real_T (*)[144])mxMalloc(sizeof(real_T [144]));
  Jgx = (real_T (*)[144])mxMalloc(sizeof(real_T [144]));
  Jfp = (real_T (*)[48])mxMalloc(sizeof(real_T [48]));
  Jgp = (real_T (*)[48])mxMalloc(sizeof(real_T [48]));

  /* Marshall function inputs */
  varargin_1 = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "varargin_1");
  varargin_2 = c_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "varargin_2");
  varargin_3 = e_emlrt_marshallIn(&st, emlrtAlias(prhs[2]), "varargin_3");

  /* Invoke the target function */
  Llamosi2016_o2(&st, varargin_1, *varargin_2, *varargin_3, *f, *g, *Jfx, *Jgx, *
                 Jfp, *Jgp);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*f);
  plhs[1] = emlrt_marshallOut(*g);
  plhs[2] = b_emlrt_marshallOut(*Jfx);
  plhs[3] = b_emlrt_marshallOut(*Jgx);
  plhs[4] = c_emlrt_marshallOut(*Jfp);
  plhs[5] = c_emlrt_marshallOut(*Jgp);
}

/* End of code generation (_coder_Llamosi2016_o2_api.c) */
