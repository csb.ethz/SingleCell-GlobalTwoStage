/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2.c
 *
 * Code generation for function 'Llamosi2016_o2'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016_o2.h"
#include "jac_Llamosi2016_o2.h"

/* Function Definitions */
void Llamosi2016_o2(const emlrtStack *sp, real_T varargin_1, const real_T
                    varargin_2[12], const real_T varargin_3[4], real_T f[12],
                    real_T g[12], real_T Jfx[144], real_T Jgx[144], real_T Jfp
                    [48], real_T Jgp[48])
{
  (void)sp;
  f[0] = varargin_3[0] * varargin_2[2] - varargin_3[1] * varargin_2[0];
  f[1] = 0.947 * varargin_2[0] - varargin_3[2] * varargin_2[1];
  f[2] = 0.3968 * ((0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 *
    varargin_1) + 300.0) + 0.5) * ((varargin_3[3] - varargin_1) + 2.0) + (0.5 *
    muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 * varargin_1) + 300.0) -
    0.5) * ((0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] - 100.0 *
    varargin_1) + 1000.0) - (0.5 * muDoubleScalarTanh((100.0 * varargin_3[3] -
    100.0 * varargin_1) + 1400.0) + 0.5) * (0.5 * muDoubleScalarTanh((100.0 *
    varargin_3[3] - 100.0 * varargin_1) + 1000.0) - 0.5) * ((0.25 * varargin_3[3]
    - 0.25 * varargin_1) + 3.5)) + 0.5)) * (0.5 * muDoubleScalarTanh((100.0 *
    varargin_3[3] - 100.0 * varargin_1) + 200.0) - 0.5) - 0.9225 * varargin_2[2];
  f[3] = (varargin_2[2] - varargin_3[1] * varargin_2[3]) + varargin_3[0] *
    varargin_2[5];
  f[4] = 0.947 * varargin_2[3] - varargin_3[2] * varargin_2[4];
  f[5] = -0.9225 * varargin_2[5];
  f[6] = (varargin_3[0] * varargin_2[8] - varargin_3[1] * varargin_2[6]) -
    varargin_2[0];
  f[7] = 0.947 * varargin_2[6] - varargin_3[2] * varargin_2[7];
  f[8] = -0.9225 * varargin_2[8];
  f[9] = varargin_3[0] * varargin_2[11] - varargin_3[1] * varargin_2[9];
  f[10] = (0.947 * varargin_2[9] - varargin_2[1]) - varargin_3[2] * varargin_2
    [10];
  f[11] = -0.9225 * varargin_2[11];
  g[0] = f[2] * varargin_3[0] - f[0] * varargin_3[1];
  g[1] = 0.947 * f[0] - f[1] * varargin_3[2];
  g[2] = -0.9225 * f[2];
  g[3] = (f[2] - f[3] * varargin_3[1]) + f[5] * varargin_3[0];
  g[4] = 0.947 * f[3] - f[4] * varargin_3[2];
  g[5] = -0.9225 * f[5];
  g[6] = (f[8] * varargin_3[0] - f[6] * varargin_3[1]) - f[0];
  g[7] = 0.947 * f[6] - f[7] * varargin_3[2];
  g[8] = -0.9225 * f[8];
  g[9] = f[11] * varargin_3[0] - f[9] * varargin_3[1];
  g[10] = (0.947 * f[9] - f[1]) - f[10] * varargin_3[2];
  g[11] = -0.9225 * f[11];
  jac_Llamosi2016_o2(varargin_2, f, varargin_3, Jfx, Jgx, Jfp, Jgp);
}

/* End of code generation (Llamosi2016_o2.c) */
