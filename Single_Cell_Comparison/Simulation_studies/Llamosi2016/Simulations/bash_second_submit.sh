#!/bin/bash
## 1. folder
## 2. subfolder
## 3. residual_type0

echo 'Trial is' $4
echo 'Data used is' $1
echo 'subfolder used:' $2
echo 'Residual:' $3

matlab -nodesktop -nosplash -r "SecondStage('$1','$2','$3',$4);exit"

