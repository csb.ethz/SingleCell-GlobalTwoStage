#!/bin/bash
#$ -V
#$ -q regular.q
#$ -o ./plot.out
#$ -e ./plot.err
#$ -cwd

matlab -nosplash  -r "plot_SAEMPreds('$1','$2',$SGE_TASK_ID); exit"
