function []=get_images_scaled(index)

if index==1

% data
data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};
% Results to GTS
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
% SAEM results
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3a';

elseif index==2
data_folder='../Simulations/Simulated_Data/Data_Fig3b/'; 
folder_names={'2','4','8','16','32'};  
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3b/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3b/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3b/'; 

elseif index==3
 data_folder='../Simulations/Simulated_Data/Data_Fig3c/';   
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3c/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3c/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3c/';
folder_names={'20','40','60','80','100'};

else
data_folder='../Simulations/Simulated_Data/Data_Fig3d/';   
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3d/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3d/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3d/'; 
folder_names={'25','50','75','100'};%
end


if index==1
label_names=[500,350,250,150,50];
else
label_names=repmat(350,1,5);
end




for j=1:length(folder_names)
    folder_name=folder_names{j};
for i=1:5
% load PL
load(sprintf('%s/%s/plots/PL_plot_data_%d.mat.',dirname1,folder_name,i))
pm0=quantile(Model_prediction,0.025);
pm100=quantile(Model_prediction, 0.975);
pMedians=quantile(Model_prediction,0.5);


% load AR
load(sprintf('%s/%s/plots/AR_plot_data_%d.mat',dirname2,folder_name,i))
m0=quantile(Model_prediction,0.025);
m100=quantile(Model_prediction, 0.975);
Medians=quantile(Model_prediction,0.5);

% Also do the same with the data
load(sprintf('%s/%s/Simulated_%d.mat',data_folder,folder_name,i))
data=reshape(table2array(Data(:,2)),101,label_names(j))';
dtime=unique(Data.TIME)';
dm0=quantile(data,0.025);
dm100=quantile(data, 0.975);
dMedians=quantile(data,0.5);

% load SAEM result
load(sprintf('%s/%s/model_mlxtran%d_SAEMPreds.mat',dirname_SAEM,folder_name,i))
sm0=quantile(Model_prediction,0.025);
sm100=quantile(Model_prediction, 0.975);
sMedians=quantile(Model_prediction,0.5);

% SAVE PL RESULT
h3=fill([0:600 fliplr(0:600)]./60,[pm0 fliplr(pm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,Medians,'Color',[0,0,0]);
ylim([min([dm0,pm0,m0,sm0]),max([sm100,pm100,m100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled',dirname1,folder_name))
set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled/PLpredictions_%d.png',dirname1,folder_name,i))

close 



% save AR result
h3=fill([0:600 fliplr(0:600)]./60,[m0 fliplr(m100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,Medians,'Color',[0,0,0]);
ylim([min([dm0,pm0,m0,sm0]),max([sm100,pm100,m100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled',dirname2,folder_name))
set(gcf, 'Units', 'normalized',   'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled/predictions_%d.png',dirname2,folder_name,i))

close 


figure
%save SAEM
h3=fill([0:600 fliplr(0:600)]./60 ,[sm0 fliplr(sm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,sMedians, 'Color',[0,0,0]);
ylim([min([dm0,pm0,m0,sm0]),max([sm100,pm100,m100,dm100])]);
set(gca,'ytick',[],'xtick',[])
mkdir(sprintf('%s/%s/images_scaled',dirname_SAEM,folder_name))
set(gcf, 'Units', 'normalized',  'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled/SAEM_Predictions_%d.png',dirname_SAEM,folder_name,i))

close 



figure
% Save data
h3=fill([dtime fliplr(dtime)]./60,[dm0 fliplr(dm100)],[0,0,0]);
alpha 1
hold on
set(h3,'EdgeColor','None');
g3=plot([dtime]./60,dMedians,'Color',[0,0,0]);
ylim([min([dm0,pm0,m0,sm0]),max([sm100,pm100,m100,dm100])]);
set(gca,'ytick',[],'xtick',[])
set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
saveas(gcf,sprintf('%s/%s/images_scaled/data_%d.png',dirname_SAEM,folder_name,i))

close 
end
end