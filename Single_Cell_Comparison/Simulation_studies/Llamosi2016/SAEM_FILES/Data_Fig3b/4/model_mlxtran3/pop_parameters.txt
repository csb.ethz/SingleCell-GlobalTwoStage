******************************************************************
*      model_mlxtran3.mlxtran
*      March 25, 2018 at 14:30:07
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38        0.038           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.26        0.035           1    
gm_pop     :    -3.59        0.054           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.682        0.029           4    
omega_kp   :        0          -           -      
omega_gp   :    0.549        0.028           5    
omega_gm   :     0.89        0.042           5    
omega_ltau :        0          -           -      
corr_km_gp :  -0.0649        0.067         104    
corr_km_gm :    0.774        0.026           3    
corr_gp_gm :   -0.653        0.039           6    

a          :     43.5         0.47           1    
b          :    0.353       0.0015           0    

correlation matrix (IIV)
km      1       
gp  -0.06       1    
gm   0.77   -0.65       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.08       1    
gm_pop   0.74    -0.7       1 

Eigenvalues (min, max, max/min): 0.019  2.1  1.1e+02

omega_km      1             
omega_gp      0       1          
omega_gm   0.54    0.48       1       
a            -0      -0      -0       1    
b         -0.01   -0.02   -0.01   -0.12       1 

Eigenvalues (min, max, max/min): 0.28  1.7  6.3

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.78   -0.05       1 

Eigenvalues (min, max, max/min): 0.0022  2.1  9.4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.52e+03 seconds. 
CPU time is 1.16e+04 seconds. 
