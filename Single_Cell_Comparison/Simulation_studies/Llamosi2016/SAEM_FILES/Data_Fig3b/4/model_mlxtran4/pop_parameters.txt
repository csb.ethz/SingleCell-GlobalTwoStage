******************************************************************
*      model_mlxtran4.mlxtran
*      March 25, 2018 at 14:56:31
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.44         0.04           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.25        0.035           1    
gm_pop     :    -3.54        0.053           1    
ltau_pop   :      3.4          -           -      

omega_km   :      0.7        0.029           4    
omega_kp   :        0          -           -      
omega_gp   :    0.558        0.028           5    
omega_gm   :    0.879        0.042           5    
omega_ltau :        0          -           -      
corr_km_gp :   0.0146        0.066         454    
corr_km_gm :    0.739        0.029           4    
corr_gp_gm :   -0.638        0.039           6    

a          :     43.6         0.47           1    
b          :    0.354       0.0015           0    

correlation matrix (IIV)
km      1       
gp   0.01       1    
gm   0.74   -0.64       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.02       1    
gm_pop   0.72   -0.69       1 

Eigenvalues (min, max, max/min): 0.017  2  1.2e+02

omega_km      1             
omega_gp     -0       1          
omega_gm   0.51    0.46       1       
a            -0      -0      -0       1    
b         -0.01   -0.02   -0.01   -0.12       1 

Eigenvalues (min, max, max/min): 0.31  1.7  5.4

corr_km_gp      1       
corr_km_gm  -0.64       1    
corr_gp_gm   0.75    0.01       1 

Eigenvalues (min, max, max/min): 0.0016  2  1.2e+03


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.54e+03 seconds. 
CPU time is 1.19e+04 seconds. 
