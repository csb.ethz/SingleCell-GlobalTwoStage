******************************************************************
*      model_mlxtran2.mlxtran
*      March 25, 2018 at 00:17:25
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.9         0.04           1    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.26        0.052           1    
gm_pop     :    -3.64        0.069           2    
ltau_pop   :      3.4          -           -      

omega_km   :    0.578        0.036           6    
omega_kp   :        0          -           -      
omega_gp   :    0.677        0.051           7    
omega_gm   :    0.906        0.069           8    
omega_ltau :        0          -           -      
corr_km_gp :  -0.0887          0.1         118    
corr_km_gm :    0.681        0.053           8    
corr_gp_gm :   -0.764        0.043           6    

a          :     43.7         0.49           1    
b          :     1.13       0.0047           0    

correlation matrix (IIV)
km      1       
gp  -0.09       1    
gm   0.68   -0.76       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.12       1    
gm_pop   0.67   -0.79       1 

Eigenvalues (min, max, max/min): 0.022  2.1  93

omega_km      1             
omega_gp     -0       1          
omega_gm   0.41    0.59       1       
a         -0.01      -0      -0       1    
b         -0.03   -0.02   -0.03   -0.04       1 

Eigenvalues (min, max, max/min): 0.28  1.7  6.1

corr_km_gp      1       
corr_km_gm  -0.78       1    
corr_gp_gm   0.72   -0.13       1 

Eigenvalues (min, max, max/min): 0.0031  2.1  6.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.53e+03 seconds. 
CPU time is 1.18e+04 seconds. 
