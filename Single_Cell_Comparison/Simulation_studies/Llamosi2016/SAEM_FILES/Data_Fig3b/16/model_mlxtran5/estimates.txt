          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.55944;   0.04328;      1.69;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.25281;   0.04552;      0.87;        NaN
    gm_pop;  -3.61704;   0.06597;      1.82;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68157;   0.03535;      5.19;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.55997;   0.04592;      8.20;        NaN
  omega_gm;   0.89879;   0.06429;      7.15;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.08672;   0.09953;    114.78;        NaN
corr_km_gm;   0.77235;   0.03665;      4.74;        NaN
corr_gp_gm;  -0.67124;   0.05654;      8.42;        NaN
         a;  42.58191;   0.47156;      1.11;        NaN
         b;   0.93010;   0.00385;      0.41;        NaN
