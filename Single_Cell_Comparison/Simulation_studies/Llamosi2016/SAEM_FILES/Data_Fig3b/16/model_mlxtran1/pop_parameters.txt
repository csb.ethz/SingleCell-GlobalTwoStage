******************************************************************
*      model_mlxtran1.mlxtran
*      March 25, 2018 at 10:37:35
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.51        0.041           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.24        0.048           1    
gm_pop     :    -3.71        0.065           2    
ltau_pop   :      3.4          -           -      

omega_km   :    0.645        0.033           5    
omega_kp   :        0          -           -      
omega_gp   :    0.578        0.048           8    
omega_gm   :    0.862        0.066           8    
omega_ltau :        0          -           -      
corr_km_gp :   0.0452          0.1         229    
corr_km_gm :     0.69        0.048           7    
corr_gp_gm :   -0.664        0.059           9    

a          :     43.2         0.48           1    
b          :    0.924       0.0038           0    

correlation matrix (IIV)
km      1       
gp   0.05       1    
gm   0.69   -0.66       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.05       1    
gm_pop   0.65   -0.77       1 

Eigenvalues (min, max, max/min): 0.019  2  1.1e+02

omega_km      1             
omega_gp  -0.01       1          
omega_gm    0.4    0.55       1       
a         -0.01      -0   -0.01       1    
b         -0.02   -0.03   -0.03   -0.05       1 

Eigenvalues (min, max, max/min): 0.31  1.7  5.4

corr_km_gp      1       
corr_km_gm  -0.67       1    
corr_gp_gm   0.77   -0.05       1 

Eigenvalues (min, max, max/min): 0.0019  2  1.1e+03


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.48e+03 seconds. 
CPU time is 1.1e+04 seconds. 
