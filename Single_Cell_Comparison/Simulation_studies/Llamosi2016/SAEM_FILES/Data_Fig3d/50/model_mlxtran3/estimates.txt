          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.35583;   0.03719;      1.58;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.31205;   0.02932;      0.55;        NaN
    gm_pop;  -3.55087;   0.04556;      1.28;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.67017;   0.02708;      4.04;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.49322;   0.02218;      4.50;        NaN
  omega_gm;   0.79962;   0.03415;      4.27;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.02757;   0.06190;    224.50;        NaN
corr_km_gm;   0.77676;   0.02359;      3.04;        NaN
corr_gp_gm;  -0.56993;   0.04180;      7.33;        NaN
         a;  61.38634;   0.70103;      1.14;        NaN
         b;   0.08665;   0.00055;      0.63;        NaN
