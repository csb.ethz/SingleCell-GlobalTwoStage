          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.32610;   0.03600;      1.55;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28729;   0.03557;      0.67;        NaN
    gm_pop;  -3.65083;   0.05050;      1.38;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.64962;   0.02627;      4.04;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.52770;   0.02797;      5.30;        NaN
  omega_gm;   0.84484;   0.03941;      4.66;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.10243;   0.06960;     67.95;        NaN
corr_km_gm;   0.77584;   0.02601;      3.35;        NaN
corr_gp_gm;  -0.67897;   0.03877;      5.71;        NaN
         a;  61.95823;   0.88448;      1.43;        NaN
         b;   0.08712;   0.00066;      0.76;        NaN
