          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.40604;   0.03857;      1.60;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.23118;   0.03344;      0.64;        NaN
    gm_pop;  -3.57870;   0.05067;      1.42;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.69922;   0.02804;      4.01;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.50371;   0.02626;      5.21;        NaN
  omega_gm;   0.86110;   0.03908;      4.54;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.08982;   0.06823;     75.97;        NaN
corr_km_gm;   0.79488;   0.02350;      2.96;        NaN
corr_gp_gm;  -0.64669;   0.04088;      6.32;        NaN
         a;  62.41568;   0.90475;      1.45;        NaN
         b;   0.08588;   0.00065;      0.76;        NaN
