******************************************************************
*      model_mlxtran2.mlxtran
*      March 04, 2018 at 02:30:54
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38         0.035          1    
kp_pop     :  -0.0545           -          -      
gp_pop     :     -5.3         0.032          1    
gm_pop     :    -3.55         0.047          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.641         0.025          4    
omega_kp   :        0           -          -      
omega_gp   :     0.55         0.024          4    
omega_gm   :     0.84         0.035          4    
omega_ltau :        0           -          -      
corr_km_gp :   -0.106         0.059         56    
corr_km_gm :     0.74         0.027          4    
corr_gp_gm :   -0.716         0.029          4    

a          :     62.2          0.65          1    
b          :   0.0859       0.00051          1    

correlation matrix (IIV)
km      1       
gp  -0.11       1    
gm   0.74   -0.72       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.09       1    
gm_pop   0.72   -0.73       1 

Eigenvalues (min, max, max/min): 0.024  2.1  85

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.51    0.53       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.27  1.7  6.5

corr_km_gp      1       
corr_km_gm  -0.71       1    
corr_gp_gm   0.74   -0.06       1 

Eigenvalues (min, max, max/min): 0.0034  2.1  6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 3.91e+03 seconds. 
CPU time is 1.62e+04 seconds. 
