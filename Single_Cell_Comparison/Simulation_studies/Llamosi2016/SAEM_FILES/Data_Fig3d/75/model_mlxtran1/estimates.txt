          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.33366;   0.03910;      1.68;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28313;   0.03103;      0.59;        NaN
    gm_pop;  -3.62637;   0.04995;      1.38;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65089;   0.02976;      4.57;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.49087;   0.02476;      5.04;        NaN
  omega_gm;   0.84092;   0.03854;      4.58;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.10348;   0.06875;     66.44;        NaN
corr_km_gm;   0.76673;   0.02789;      3.64;        NaN
corr_gp_gm;  -0.68553;   0.03663;      5.34;        NaN
         a;  63.15590;   0.79026;      1.25;        NaN
         b;   0.08689;   0.00061;      0.70;        NaN
