******************************************************************
*      model_mlxtran2.mlxtran
*      March 04, 2018 at 10:30:53
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.4        0.038           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.29        0.032           1    
gm_pop     :    -3.53        0.046           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.626        0.029           5    
omega_kp   :        0          -           -      
omega_gp   :    0.513        0.025           5    
omega_gm   :    0.775        0.036           5    
omega_ltau :        0          -           -      
corr_km_gp :   -0.012        0.068         570    
corr_km_gm :    0.734        0.031           4    
corr_gp_gm :   -0.651        0.039           6    

a          :     62.4         0.81           1    
b          :   0.0859       0.0006           1    

correlation matrix (IIV)
km      1       
gp  -0.01       1    
gm   0.73   -0.65       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.05       1    
gm_pop   0.72   -0.63       1 

Eigenvalues (min, max, max/min): 0.022  1.9  89

omega_km      1             
omega_gp  -0.01       1          
omega_gm   0.51     0.4       1       
a            -0      -0      -0       1    
b         -0.01   -0.02   -0.02   -0.39       1 

Eigenvalues (min, max, max/min): 0.35  1.6  4.8

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.73    0.02       1 

Eigenvalues (min, max, max/min): 0.003  2  6.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.5e+03 seconds. 
CPU time is 1.87e+04 seconds. 
