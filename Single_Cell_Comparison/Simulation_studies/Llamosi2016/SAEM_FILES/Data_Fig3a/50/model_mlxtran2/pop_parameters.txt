******************************************************************
*      model_mlxtran2.mlxtran
*      July 19, 2017 at 21:11:27
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.43         0.061          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.06         0.064          1    
gm_pop     :    -3.72         0.071          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.428         0.043         10    
omega_kp   :        0           -          -      
omega_gp   :    0.449         0.046         10    
omega_gm   :    0.496          0.05         10    
omega_ltau :        0           -          -      
corr_km_gp :   -0.126          0.14        112    
corr_km_gm :     0.71         0.071         10    
corr_gp_gm :   -0.746         0.063          8    

a          :     14.2          0.38          3    
b          :   0.0229       0.00032          1    

correlation matrix (IIV)
km      1       
gp  -0.13       1    
gm   0.71   -0.75       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.12       1    
gm_pop    0.7   -0.75       1 

Eigenvalues (min, max, max/min): 0.032  2.1  66

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.49    0.56       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.36       1 

Eigenvalues (min, max, max/min): 0.26  1.8  6.8

corr_km_gp      1       
corr_km_gm  -0.74       1    
corr_gp_gm    0.7   -0.05       1 

Eigenvalues (min, max, max/min): 0.0065  2  3.1e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 898 seconds. 
CPU time is 3.51e+03 seconds. 
