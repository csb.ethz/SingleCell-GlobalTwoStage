          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.25177;   0.07676;      3.41;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.12397;   0.06610;      1.29;        NaN
    gm_pop;  -3.77539;   0.07798;      2.07;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.54241;   0.05432;     10.01;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.46369;   0.04708;     10.15;        NaN
  omega_gm;   0.54742;   0.05553;     10.14;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.06301;   0.14210;    225.50;        NaN
corr_km_gm;   0.74206;   0.06458;      8.70;        NaN
corr_gp_gm;  -0.66340;   0.07995;     12.05;        NaN
         a;  13.88528;   0.36516;      2.63;        NaN
         b;   0.02241;   0.00032;      1.41;        NaN
