******************************************************************
*      model_mlxtran3.mlxtran
*      March 02, 2018 at 14:01:37
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.46         0.047          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.26         0.033          1    
gm_pop     :    -3.53          0.06          2    
ltau_pop   :      3.4           -          -      

omega_km   :     0.73         0.033          5    
omega_kp   :        0           -          -      
omega_gp   :    0.501         0.025          5    
omega_gm   :    0.921         0.043          5    
omega_ltau :        0           -          -      
corr_km_gp :   -0.189         0.065         35    
corr_km_gm :    0.838          0.02          2    
corr_gp_gm :   -0.664         0.038          6    

a          :     61.9          0.73          1    
b          :   0.0863       0.00055          1    

correlation matrix (IIV)
km      1       
gp  -0.19       1    
gm   0.84   -0.66       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.18       1    
gm_pop   0.82   -0.68       1 

Eigenvalues (min, max, max/min): 0.018  2.2  1.2e+02

omega_km      1             
omega_gp   0.03       1          
omega_gm   0.68    0.45       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.2  1.8  9.2

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.83   -0.14       1 

Eigenvalues (min, max, max/min): 0.0027  2.1  8e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 6.06e+03 seconds. 
CPU time is 1.67e+04 seconds. 
