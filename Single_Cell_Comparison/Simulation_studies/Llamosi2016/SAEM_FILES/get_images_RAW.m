function []=get_images_RAW(index)
% PLOT the predictiosn of the simulated data. Save the quantiles and the pn
%g images. 


if index==1

% data
data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};
% Results to GTS
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
% SAEM results
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3a';

elseif index==2
data_folder='../Simulations/Simulated_Data/Data_sigmas/'; 
folder_names={'2','4','8','16','32'};  
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_sigmas/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_sigmas/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3b/'; 

elseif index==3
 data_folder='../Simulations/Simulated_Data/Data_scrambleds/';   
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_scrambleds/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_scrambleds/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3c/';
folder_names={'20','40','60','80','100'};

else
data_folder='../Simulations/Simulated_Data/Data_scrambled_bios/';   
dirname1='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_scrambled_bios/';
dirname2='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_scrambled_bios/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3d/'; 
folder_names={'25','50','75','100'};%
end


if index==1
label_names=[500,350,250,150,50];
else
label_names=repmat(350,1,5);
end

med_diffs=[];
q25_diffs=[];
q75_diffs=[];
med_adiffs=[];
q25_adiffs=[];
q75_adiffs=[];

for j=1:length(folder_names)
    folder_name=folder_names{j};
 
for i=1:5
%% PL results
load(sprintf('%s/%s/plots/PL_plot_data_%d.mat.',dirname1,folder_name,i))
Model_prediction(isinf(Model_prediction))=nan;
pm0=quantile(Model_prediction,0.025);
pm100=quantile(Model_prediction, 0.975);
pMedians=quantile(Model_prediction,0.5);


%% AR results
load(sprintf('%s/%s/plots/AR_plot_data_%d.mat',dirname2,folder_name,i))

figure
Model_prediction2(isinf(Model_prediction))=nan;
m0=quantile(Model_prediction,0.025);
m100=quantile(Model_prediction, 0.975);
Medians=quantile(Model_prediction,0.5);

%% Also do the same with the data
load(sprintf('%s/%s/Simulated_%d.mat',data_folder,folder_name,i))

data=reshape(table2array(Data(:,2)),101,label_names(j))';
dtime=unique(Data.TIME)';
dm0=quantile(data,0.025);
dm100=quantile(data, 0.975);
dMedians=quantile(data,0.5);

%% SAEM results
load(sprintf('%s/%s/model_mlxtran%d_SAEMPreds.mat',dirname_SAEM,folder_name,i))
sm0=quantile(Model_prediction,0.025);
sm100=quantile(Model_prediction, 0.975);
sMedians=quantile(Model_prediction,0.5);


%% PLot things
% PL plot
h3=fill([0:600 fliplr(0:600)]./60,[pm0 fliplr(pm100)],[0,0.5,0]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,Medians,'Color',[0,0.5,0]);
ylim([min([pm0,dm0]),max([pm100,dm100])]);

mkdir(sprintf('%s/%s/images_raw',dirname1,folder_name))
%set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
h3=fill([dtime fliplr(dtime)]./60,[dm0 fliplr(dm100)],[0,0,0]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([dtime]./60,dMedians,'Color',[0,0,0]);
ylim([min([dm0,pm0]),max([pm100,dm100])]);
%set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
legend('GTS-PL: 95% quantiles','GTS-PL: median','Data: quantiles','Data:median')
xlabel('Time [h]')
ylabel('Protein [AU]')
saveas(gcf,sprintf('%s/%s/images_raw/PLpredictions_%d.png',dirname1,folder_name,i))

close 

% AR plot
h3=fill([0:600 fliplr(0:600)]./60,[m0 fliplr(m100)],[0.5,0,0]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,Medians,'Color',[0.5,0,0]);
ylim([min([m0,dm0]),max([m100,dm100])]);
mkdir(sprintf('%s/%s/images_raw',dirname2,folder_name))
%set(gcf, 'Units', 'normalized',   'Position', [0 0 1 1])
h3=fill([dtime fliplr(dtime)]./60,[dm0 fliplr(dm100)],[0,0,0]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([dtime]./60,dMedians,'Color',[0,0,0]);
ylim([min([dm0,m0]),max([dm100,m100])]);
%set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
legend('GTS-AR: 95% quantiles','GTS-AR: median','Data: quantiles','Data:median')
xlabel('Time [h]')
ylabel('Protein [AU]')
saveas(gcf,sprintf('%s/%s/images_raw/ARpredictions_%d.png',dirname2,folder_name,i))

close 

% SAEM plot
figure
h3=fill([0:600 fliplr(0:600)]./60 ,[sm0 fliplr(sm100)],[0,0,0.5]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([0:600]./60,sMedians, 'Color',[0,0,.5]);
ylim([min([m0,sm0]),max([sm100,dm100])]);
%set(gcf, 'Units', 'normalized',  'Position', [0 0 1 1])

h3=fill([dtime fliplr(dtime)]./60,[dm0 fliplr(dm100)],[0,0,0]);
alpha 0.5
hold on
set(h3,'EdgeColor','None');
g3=plot([dtime]./60,dMedians,'Color',[0,0,0]);
ylim([min([dm0,sm0]),max([sm100,dm100])]);
legend('SAEM: 95% quantiles','SAEM: median','Data: quantiles','Data:median')
xlabel('Time [h]')
ylabel('Protein [AU]')
%set(gcf, 'Units', 'normalized', 'Position', [0 0 1 1])
mkdir(sprintf('%s/%s/images_raw',dirname_SAEM,folder_name))
times=0:600;
med_adiffs=cat(1,med_adiffs,[nansum(abs(sMedians(ismember(times,dtime))-dMedians)),nansum(abs(Medians(ismember(times,dtime))-dMedians))]);
q75_adiffs=cat(1,q75_adiffs,[nansum(abs(sm100(ismember(times,dtime))-dm100)),nansum(abs(m100(ismember(times,dtime))-dm100))]);
q25_adiffs=cat(1,q25_adiffs,[nansum(abs(sm0(ismember(times,dtime))-dm0)),nansum(abs(m0(ismember(times,dtime))-dm0))]);

med_diffs=cat(1,med_diffs,[sum(abs((sMedians(ismember(times,dtime))-dMedians)./(dMedians+1e-3))),sum(abs((Medians(ismember(times,dtime))-dMedians)./(dMedians+1e-3)))]);
q75_diffs=cat(1,q75_diffs,[sum(abs((sm100(ismember(times,dtime))-dm100)./dm100)),sum(abs((m100(ismember(times,dtime))-dm100)./dm100))]);
q25_diffs=cat(1,q25_diffs,[sum(abs((sm0(ismember(times,dtime))-dm0)./(dm0+1e-3))),sum(abs((m0(ismember(times,dtime))-dm0)./(dm0+1e-3)))]);


saveas(gcf,sprintf('%s/%s/images_raw/SAEMdata_%d.png',dirname_SAEM,folder_name,i))

close 

end
end
save(sprintf('meds_%d.mat',index),'med_diffs','q25_diffs','q75_diffs','med_adiffs','q25_adiffs','q75_adiffs')