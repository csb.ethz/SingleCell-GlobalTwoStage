function []=plot_SAEMPreds(folder,subfolder,trial)
% This function integrates ODE model based on the results from SAEM. 
% The models were fit to simulated data using MONOLIX. 

addpath(genpath('~/Repos/odeSD/')) % integrator
addpath(genpath('../Simulations/Model')) % ODE model

% Result folders
mlxfols={'model_mlxtran1','model_mlxtran2','model_mlxtran3','model_mlxtran4','model_mlxtran5'}
mfols=sprintf('%s/%s',folder,subfolder);
ii=trial;

estimates=readtable(sprintf('%s/%s/estimates.txt',mfols,mlxfols{ii}));
Mean=estimates.parameter([1,4,3]);
tau=estimates.parameter(5);
Dhat=estimates.parameter([6,9,8]);
Correl=estimates.parameter([12,11,13]);
Corr=[1,Correl(1),Correl(2),Correl(1),1,Correl(3),Correl(2),Correl(3),1];
Corr=reshape(Corr,3,3);
e_b=estimates.parameter(15);
e_a=estimates.parameter(14);
Model_prediction=[];
params0=[];
Dhat=corr2cov(Dhat, Corr);
betahat=Mean;

ME=[]; % Error holder
for i=1:10000
try
paras0= exp(mvnrnd((betahat),round(Dhat,4))); 
catch ME
end

int_opts.abstol=1e-6;
int_opts.reltol=1e-4;


params0=cat(1,params0,paras0);
[f_t]=Run_Llamosi2016(0:600,[paras0(1:2),paras0(3)],int_opts);
f_t=f_t+(e_a+f_t.*e_b).*randn(size(f_t));
Model_prediction=cat(1,Model_prediction,f_t);
end

save(sprintf('%s/%s_SAEMPreds.mat',mfols,mlxfols{ii}),'Model_prediction','Dhat','betahat')
end

