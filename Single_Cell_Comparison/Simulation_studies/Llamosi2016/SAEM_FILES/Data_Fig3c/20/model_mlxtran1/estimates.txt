          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.35136;   0.03691;      1.57;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.27471;   0.03111;      0.59;        NaN
    gm_pop;  -3.63522;   0.04947;      1.36;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68026;   0.02647;      3.89;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.54249;   0.02311;      4.26;        NaN
  omega_gm;   0.89119;   0.03622;      4.06;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.08736;   0.05779;     66.16;        NaN
corr_km_gm;   0.78268;   0.02256;      2.88;        NaN
corr_gp_gm;  -0.65728;   0.03295;      5.01;        NaN
         a;  62.55397;   0.62056;      0.99;        NaN
         b;   0.09533;   0.00051;      0.53;        NaN
