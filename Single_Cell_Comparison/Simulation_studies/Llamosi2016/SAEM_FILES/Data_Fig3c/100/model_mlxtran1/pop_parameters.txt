******************************************************************
*      model_mlxtran1.mlxtran
*      March 07, 2018 at 04:17:10
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.36         0.037          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.033          1    
gm_pop     :     -3.6         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.675         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.575         0.024          4    
omega_gm   :    0.856         0.035          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0564         0.058        103    
corr_km_gm :    0.785         0.023          3    
corr_gp_gm :   -0.531         0.042          8    

a          :     65.8          0.66          1    
b          :    0.114       0.00059          1    

correlation matrix (IIV)
km      1       
gp   0.06       1    
gm   0.79   -0.53       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.05       1    
gm_pop   0.76   -0.57       1 

Eigenvalues (min, max, max/min): 0.027  1.9  72

omega_km      1             
omega_gp      0       1          
omega_gm   0.58    0.31       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.34       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.54       1    
corr_gp_gm   0.78    0.09       1 

Eigenvalues (min, max, max/min): 0.0039  1.9  4.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.59e+03 seconds. 
CPU time is 1.95e+04 seconds. 
