% %%%------------------------------------------------------------------------
% Run the nlmefitsa function in MATLAB on llamosi's model and data set
% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4747589/

% The native SAEM impementation in MATLAB takes a LONG time. 
% We did not wait for the result from this code. 
% Using nlmefit did not work on this data, as it does not support NaNs

% Model folder contains the model files. 
% Data contains the data used (same as that used by Monolix). 

% Lekshmi D. 2018
%%%-----------------------------------------------------------------------
 
addpath(genpath('~/Repos/odeSD'))
addpath(genpath('./Model'))

%% data, model integration options
data_all=readtable('./Data/Di_monolix_full_dnan.txt','TreatAsEmpty','.');
data=data_all(~isnan(data_all.Y),:);
int_opts.abstol=1e-4;
int_opts.reltol=1e-4;
t1=tic;
cptime=cputime();


%% Use nlmefitsa: TAKES A LONG TIME
% function handle to ODE mdoel
model = @(phi,t) Run_Llamosi2016wosens(t,phi,int_opts);

% intialise options for NMEFITSA
phi0 =exp([2.3,-1.22,-5.52]);
xform = ones(1,length(phi0)); 
%Options=statset('Display','iter');

% setting a global seed. 
rng(100);

[beta,PSI,stats,br] = nlmefitsa(data.TIME,data.Y,...
   data.ID,[],model,phi0,'ParamTransform',xform,'CovPattern',ones(3),'REParamsSelect',[1,2,3],'ErrorModel','combined');
t2=toc(t1);
cptime2=cputime();

% save results
betahat=beta; Dhat=PSI; betahati=br;
comp_time=cptime2-cptime;
save('./Pubs_Results/nlmefit_Result.mat', 'betahat','Dhat','betahati','comp_time','phi0','stats')

