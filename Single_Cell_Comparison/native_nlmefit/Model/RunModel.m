% This file compiles the c file to mex model that
% is called by ODESD, after getting the c files from 
%  .txt file


%Add path
addpath(genpath('/home/dlekshmi/TOOLBOXES/IQM Tools V1.2.1'))
addpath(genpath('/home/dlekshmi/TOOLBOXES/libSBML-5.11.4-matlab'))
addpath(genpath('~/Repos/odeSD'))

% Convert from .txt model to m and c models
generateRhs('Example_model_reduced.txt',{'C', 'Cpp', 'MatlabMex', 'Matlab'},'./',struct( 'doNotSubstitute', {{'u_c','k_p','tau'}}) )

% Directly compiling ODESDHybrid solver
odeSD_compile_hybrid('Example_model_reduced.c','Example_model_reduced','./')
