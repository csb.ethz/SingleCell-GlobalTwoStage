/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_initialize.c
 *
 * Code generation for function 'Llamosi2016_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016.h"
#include "Llamosi2016_initialize.h"
#include "_coder_Llamosi2016_mex.h"
#include "Llamosi2016_data.h"

/* Function Definitions */
void Llamosi2016_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

/* End of code generation (Llamosi2016_initialize.c) */
