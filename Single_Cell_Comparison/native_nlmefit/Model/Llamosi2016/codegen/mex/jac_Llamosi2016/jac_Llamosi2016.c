/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016.c
 *
 * Code generation for function 'jac_Llamosi2016'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016.h"

/* Function Definitions */
void jac_Llamosi2016(real_T varargin_1, const real_T varargin_2[3], const real_T
                     varargin_3[4], real_T Jfx[9], real_T Jgx[9], real_T Jfp[12],
                     real_T Jgp[12])
{
  int32_T i0;
  static const real_T dv0[9] = { 6.4E-5, -0.015151999999999999, 0.0, 0.0, 6.4E-5,
    0.0, -4.159335, 4.23309, 0.85100625 };

  int32_T i1;
  int32_T i2;
  static const real_T a[9] = { -0.008, 0.947, 0.0, 0.0, -0.008, 0.0, 4.47, 0.0,
    -0.9225 };

  (void)varargin_1;

  /*  odeSD */
  for (i0 = 0; i0 < 9; i0++) {
    Jgx[i0] = dv0[i0];
    Jfx[i0] = 0.0;
  }

  Jfx[0] = -0.008;
  Jfx[1] = 0.947;
  Jfx[4] = -0.008;
  Jfx[6] = 4.47;
  Jfx[8] = -0.9225;
  memset(&Jfp[0], 0, 12U * sizeof(real_T));
  Jfp[0] = varargin_2[2];
  Jfp[3] = -varargin_2[0];
  Jfp[7] = -varargin_2[1];
  for (i0 = 0; i0 < 3; i0++) {
    for (i1 = 0; i1 < 4; i1++) {
      Jgp[i0 + 3 * i1] = 0.0;
      for (i2 = 0; i2 < 3; i2++) {
        Jgp[i0 + 3 * i1] += a[i0 + 3 * i2] * Jfp[i2 + 3 * i1];
      }
    }
  }

  Jgp[0] += varargin_3[2];
  Jgp[3] += -varargin_3[0];
  Jgp[7] += -varargin_3[1];
}

/* End of code generation (jac_Llamosi2016.c) */
