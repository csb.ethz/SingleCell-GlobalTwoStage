/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_info.c
 *
 * Code generation for function '_coder_jac_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016.h"
#include "_coder_jac_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("jac_Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[15] = {
    "789ced5b4d6f1b4518dea0128a50ab8204a8080ab920a4aa59a709c1bed55fb19dc65fb19da48e60b51fb3f6646777ec9df54772ca1189034515020e483df6c4"
    "0524c40d890b176e45e207f00ff803ecf823d94eb6ddd46b3b6ed79656f6bbf63bfbbccfceb3f3be33636e2193e5ecd755fbf87d8be316edf7cbf6f10ad77fbd",
    "3ab017ece39dc17bfffc25eecac0feda3e646c58a06bf5bf34441d70c3978275688886553e6c00ce0404a336507adfa8108132d4c116761869681bfa86e3ab13"
    "837e453fc7eb40d64a2d9d33ebe4e4321c721abd781e71a7f15c7289a7e988e7dac0de4f7eced7b10e7805018dd475c86f2151c74468987c1421410608113ed3",
    "d4f912346a080871fb8410c77a433421c1c6e0e750b81d5a59e7b358016878aa77e640940587bdac3bf0863df05e66f0525b3715d8860a388fff22e3bfd8bb3b"
    "2d0981fef5bff4f0af30fed4decf6cedd99419c0e22572ab6642857478d2b9658206b6cf10c067a3e5ad684cd8a6e14abc8531927097073ae21194785db49028",
    "f1b841f86128cb0d173e165df02c38f0bc3e38cf71c7bffef8efdfd1d1fdddefc773f8ff10bb3e9eeba73dee87538fd706766775c73ccce52ab286a351b3566a"
    "c5c2ca6ebcdfde278ef6165cdae31cefa3fc7e96fb8f1b9ed7183cd41eb4712e7fbf7a2a33fee5f1f231a4431fde9f11f5f460b178a17a1a9bbf979ede66ee07",
    "b5c39da25ad95b5bff2cdd46db914438bc852b32371b7a9a76ff2978e0f990c1436dd91e01cd65682707a621a26548622d88ac8c916be9c084f25874f6bd87bf"
    "ccf8cb3e79a2c7cd5e60fccd61643c1bd96064f7d16f7fb9fad7e340e8ce6d1c4b20b8228613b58d3529d20a2b79435f8f15d273ddb9e1f988c1436d46774416",
    "11e8367a79aa056de54c43770ae3aff8e4c955776722f33bde710fc695bf5db4ff28ba93baf772dbaaa8ed14b377739bdbc02a854af980eace8bbfb7183cd466"
    "746737791adfa4f3c91dc67fc7273fae7ab323a20cf9c827bf0a4a3ee9a6af6cc50addcbec460da3b20d0ba498ab917233391d7d3df2c03b6bf3215efcbec9e0",
    "a5b6dd6705dd823a20421da006301ded859ed1def0e56c6fe877df03c7178c1fb5c7f85c3a13924ffd3dfef9b7e0eaaf5abbab46e15112146a8964a4a8173743"
    "24959a8efe1e7ae0450c5e6a4fb61f2dc958d7b121c87446950c71ceebbc799d17843a6f569feb5e3cbecbe0a2b64bbd279ac9da5874e7c5d33ee3bfef93a7a7",
    "d6793422ff7abbf64f3eb8e35f3e958a37d7f225825676779beb8a4460114e697d6056fbd11d0f5c57185cd486c4e83fff2dbaac399eface8b9f2ae35ff5c90f"
    "7d1ea9b00b9406b6e9e19f08699888fbe8a7df06791e25b5aa1d969a3588e2e5b8b6dd8cd5ca5a259908b6ce4659df864485f698597f51d7b79fd0573f941e23",
    "735d8db61ed76e8264a32d93c85e6715e30229adacdeab72d3d1d54cd41fa7ba5a52a1492c159ee0f3aadb6e30f8a84d9350bb6d41c526c2b821e0363055843b"
    "fd92d0dfbcc9430f3c1ae3a7f9e4cb915f0fe87b4668bef3c8e3ef82a243b7f10dc7bbcda29aad46eb264e98c92d6bb793ad4e298ffcd303ef370c5e6a4fb15f",
    "2d3dfb07cf372fb93038771acfa5de9838a63a111a0ae8660ceb5ce3639169af38065e99f1f1a9fbe1e6e3e3597f375d166e974347854363afdb3c583ddc4c1f"
    "944b52353d1beb77d3ee3f773cf09cb7ae7b5e7dca75d19cd77367fcfd5e7f6cfe2f5a3df7b28d77a3d47176fbbad8f5a7c78ba8ff863c22fb03df0fc26ffd17",
    "e8bc1375d7ab2074900f55602c54ee1cad886b685aeb05731df675088d974087d098eb901b5d879f66b25a43cb81ae95ce966a85d4dd422a8462f379183ff5dc"
    "a4e737e7f5dbecebea22ebb7595d0ff79ad7bccee0a23633cf2121912c776b4077f6cf49d575e3e6c9f5f9739386c4f742f2b91feca78fef07576f751d85d5f0",
    "5a4cd32245a0cae5d6ea1e88265f4ebd9db71f79e9ed3d0617b5ddf4060d040d701aefa4f65f0a8c9f30399efa21d944f9e8af37fe3b0eee7eaf982446492cb7"
    "5668a370d1b0acf0aed66c4e69ffe58baab773ecb7ece9ad45806002957ef6a5b799f85f4f9f274748beffd71368dd99d1ec5a29aac6f3729290037127538f24",
    "f429add73df4c06b3078a93d59dd2d4980ce8b587513903a46caf974f8018393da6e3a7434eb2bcff4d2a1c4e09126c7db49486358478887fe086ebe99557622"
    "f95cb895dad854724d9c2f541247ab01cf37bd789cef779eef771e556fb3bcdf7956f3cdf7195cd466f436ccc94ea6542639ce890c1e71223c0d423aa5eae43e",
    "8e38aff2c6b1165cdda920d689e0c6ca5eb3d8b0c225bdded98876a6a0bbff01bdc8687e",
    "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 19664U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_jac_Llamosi2016_info.c) */
